<?php
/** @var $this SenderViewRequests */
defined('_JEXEC') or die;// No direct access
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
$user = JFactory::getUser();
$userId = $user->get('id');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'ordering';

if ($saveOrder) {
	$saveOrderingUrl = 'index.php?option=com_sender&task=requests.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'articleList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
    Joomla.orderTable = function () {
        var table = document.getElementById("sortTable");
        var direction = document.getElementById("directionTable");
        var order = table.options[table.selectedIndex].value;
        if (order != '<?php echo $listOrder; ?>') {
            dirn = 'asc';
        } else {
            dirn = direction.options[direction.selectedIndex].value;
        }
        Joomla.tableOrdering(order, dirn, '');
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_sender&view=requests'); ?>" method="post" name="adminForm"
      id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
		<?php else : ?>
        <div id="j-main-container">
			<?php endif; ?>

			<?php
			// Search tools bar
			echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
			?>

            <table class="table table-striped" id="articleList">
                <thead>
                <tr>
                    <th width="1%" class="hidden-phone">
                        <input type="checkbox" name="checkall-toggle" value=""
                               title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
                    </th>
                    <th width="1%" class="nowrap hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?>
                    </th>
                    <th width="5%" style="min-width:55px" class="hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'Дата', 'date', $listDirn, $listOrder); ?>
                    </th>
                    <th width="15%">
						<?php echo JHtml::_('searchtools.sort', 'ФИО', 'name', $listDirn, $listOrder); ?>
                    </th>
                    <th width="10%" class="hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'Телефон', 'phone', $listDirn, $listOrder); ?>
                    </th>
                    <th width="10%" class="hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'ИНН', 'inn', $listDirn, $listOrder); ?>
                    </th>
                    <th width="5%" class="hidden-phone">
						<?php echo JText::_('Альфа-адмитад'); ?>
                    </th>
                    <th width="5%" class="hidden-phone">
						<?php echo JText::_('СМС'); ?>
                    </th>
                    <th width="1%" class="nowrap hidden-phone">
						<?php echo JHtml::_('searchtools.sort', 'Адмитад', 'admitad', $listDirn, $listOrder); ?>
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($this->items as $i => $item) :
					$item->max_ordering = 0;
					$ordering = ($listOrder == 'a.ordering');
					?>
                    <tr class="row<?php echo $i % 2; ?>">
                        <td class="center hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
                        </td>
                        <td class="center hidden-phone">
                            <a href="<?php echo JRoute::_('index.php?option=com_sender&task=request.edit&id=' . $item->id); ?>"
                               title="<?php echo JText::_('JACTION_EDIT'); ?>">
								<?php echo $this->escape($item->id); ?></a>
                        </td>
                        <td class="nowrap small hidden-phone">
							<?php echo JHtml::_('date', $item->date, JText::_('DATE_FORMAT_LC6')); ?>
                        </td>
                        <td class="small hidden-phone">
							<?php echo $this->escape($item->name); ?>
                        </td>
                        <td class="small hidden-phone">
							<?php echo $this->escape($item->phone); ?>
                        </td>
                        <td class="small hidden-phone">
							<?php echo $this->escape($item->inn); ?>
                        </td>
                        <td class="small hidden-phone">
							<?php if (stristr($item->alfa_result, "Успешно")) {
								$class = 'icon-publish';
								$text = 'Принят';
							} elseif (stristr($item->alfa_result, "Отказано") || stristr($item->alfa_result, "Дубль")) {
								$class = 'icon-smiley-sad';
								$text = 'Отклонен';
							} elseif (stristr($item->alfa_result, "Ошибка")) {
								$class = 'icon-unpublish';
								$text = 'Ошибка';
							} else {
								$class = 'icon-clock';
								$text = 'Данных нет';
							}
							?>
                            <span class="<?php echo $class; ?>" title="<?php echo $text; ?>"></span>

                        </td>
                        <td class="hidden-phone">

							<?php
                            if (stristr($item->alfa_result, "Успешно")) {
								if (!$item->confirm_result) {
									$class = 'icon-ban-circle';
									$text = 'Не ввел смс';
								} elseif (stristr($item->confirm_result, "Успешно") || stristr($item->confirm_result, "УСПЕШНО")) {
									$class = 'icon-publish';
									$text = 'Принят';
								} else {
									$class = 'icon-unpublish';
									$text = 'Ошибка';
								}
								?>
                                <span class="<?php echo $class; ?>" title="<?php echo $text; ?>"></span>
							<?php } ?>
                        </td>
                        <td class="center">
							<?php if (isset($item->admitad) && $item->admitad) {
								foreach ($item->admitad as $key => $results) :
									switch ($results->result) {
										case 1:
											$class = 'icon-publish';
											$text = 'Принят';
											break;
										case 2:
											$class = 'icon-smiley-sad';
											$text = 'Отклонен';
											break;
										case 3:
											$class = 'icon-unpublish';
											$text = 'Ошибка';
											break;
										default:
											$class = 'icon-clock';
											$text = 'Данных нет';
											break;
									}
									?>
                                    <span class="<?php echo $class; ?>" title="<?php echo $results->name; ?>"></span>
								<?php endforeach;
							} ?>
                        </td>

                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
			<?php echo $this->pagination->getListFooter(); ?>

            <input type="hidden" name="task" value=""/>
            <input type="hidden" name="boxchecked" value="0"/>
			<?php echo JHtml::_('form.token'); ?>

        </div>
</form>