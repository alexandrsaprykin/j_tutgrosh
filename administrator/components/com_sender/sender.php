<?php
defined( '_JEXEC' ) or die; // No direct access
/**
 * Component sender
 * @author Sapaleks
 */
require_once JPATH_COMPONENT.'/helpers/sender.php';
$controller = JControllerLegacy::getInstance( 'sender' );
$controller->execute( JFactory::getApplication()->input->get( 'task' ) );
$controller->redirect();