<?php

// No direct access
defined('_JEXEC') or die;

/**
 * Модель рассылки запросов
 * @author Sapaleks
 */
class SenderModelSender extends JModelList
{

	public function admitadAlfaSend($data)
	{
		$name = explode(" ", $data['contact_name']);
		$lastName = (isset($name[0]) ? $name[0] : $data['contact_name']);
		$firstName = (isset($name[1]) ? $name[1] : "");
		$middleName = (isset($name[2]) ? $name[2] : "");
		$fio = $data['contact_name'];
		$phone = $data['phone1'];
		$inn = $data['idencod'];
		$sum = $data['summa'];
		if (strpos($sum, '-')) $sum = substr($sum, 0, strpos($sum, '-'));
		if (strpos($sum, '.')) $sum = substr($sum, 0, strpos($sum, '.'));
		if ($data['tip'] == "Я работаю неофициально") $trud = 9;
		if ($data['tip'] == "Я не работаю") $trud = 3;
		if ($data['tip'] == "Я частный предприниматель") $trud = 1;
		if ($data['tip'] == "Я получаю пенсию") $trud = 4;
		if ($data['tip'] == "Я работаю официально") $trud = 5;
		if ($data['tip'] == "Я военнослужащий") $trud = 5;


		$data['istok'] = str_replace('-', '', $data['istok']);
		$apsource = 'C24' . $data['istok'];
		$partner = 'https://ad.admitad.com/g/5351d3d56e053247b61be6dcee139a0ef8bffa25/?subid=' . $data['istok'] . '&subid1=' . $phone . '&subid2=' . $inn . '&subid3=' . $sum . '&subid4=' . $tip2;
		$urlApi = 'https://alfa-credits.com.ua/card/submit/';
		$data = array(
			"first_name" => ($firstName),
			"last_name" => ($lastName),
			"inn" => $inn,
			"phone" => $phone,
			"placement" => $trud,
			"url" => $partner,
			"confirmation" => 1,

		);
		$request = $urlApi . "?" . http_build_query($data);
//var_dump($request);
		/* $curl = curl_init();

		 if ($curl) {
			 curl_setopt($curl, CURLOPT_URL, $request);
			 curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			 $out = curl_exec($curl);
			 curl_close($curl);
		 }
		 $out = json_decode($out, true);*/
		$out = json_decode(file_get_contents($request), true);

		$result_text = '';
		//$out['status'] = 'sended sms';
		if ($out['status'] == 'sended sms') {
			/*if((!is_numeric($data['summa']) || $data['summa'] > 1000000) and ($tip2 = "B3m" or $tip2 = "CPB1g" or $tip2 = "P") and $data['obl'] != 'Черновицкая область' and $data['obl'] != 'Автономная Республика Крым' and
				$data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область' and $data['age'] != "до 21 года" and $data['age'] >= 21){
				to_log('отправка в Адмитад-Альфа. Заявка от ' . $data['contact_name'] . '. Результат: Успешно. Заявка больше 1 000 000');
				$result = 'Успешно (Заявка больше 1 000 000)';
			}else{*/
			//to_log('отправка в Адмитад-Альфа. Заявка от ' . $data['contact_name'] . '. Результат: Успешно. Ожидает подтверждения по смс');
			$result = 'Успешно';
			$result_text = 'Успешно. Ожидает подтверждения по смс';
			//}

		} elseif ($out['status'] == 'error') {
			if (!is_array($out['error']) && stristr($out['error'], "It took less than 30 days")) {
				//to_log('отправка в Адмитад-Альфа. Заявка от ' . $data['contact_name'] . '. Результат: Дубль(' .$out['error'] . ')');
				$result = 'Дубль';
				$result_text = 'Дубль(' .$out['error'] . ')';
			} else {
				//to_log('отправка в Адмитад-Альфа. Заявка от ' . $data['contact_name'] . '. Результат: Ошибка. Текст: ' . (is_array($out['error'])? urldecode(http_build_query($out['error'])) : $out['error']));
				$result = 'Ошибка: ' . (is_array($out['error']) ? urldecode(http_build_query($out['error'])) : $out['error']);
				$result_text = 'Ошибка: ' . (is_array($out['error']) ? urldecode(http_build_query($out['error'])) : $out['error']);
			}
		} elseif ($out['status'] == 'success') {
			//to_log('отправка в Адмитад-Альфа. Заявка от ' . $data['contact_name'] . '. Результат: Отказано (банк не принял заявку)');
			$result = 'Отказано';
			$result_text = 'Отказано (банк не принял заявку)';
		} else {
			//to_log('отправка в Адмитад-Альфа. Заявка от ' . $data['contact_name'] . '. Результат: неизвестный результат: ' . $out['status']);
			$result = 'Неизвестный результат: ' . $out['status'];
			$result_text = 'Неизвестный результат: ' . $out['status'];
		}


		// }else to_log('Не отправлено в Адмитад-Альфа (сумма больше 1 000 000)');
		return array('out' => $out, 'result' => $result, 'result_text' => $result_text);
	}

}