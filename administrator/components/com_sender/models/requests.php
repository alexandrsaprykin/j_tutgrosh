<?php

// No direct access
defined('_JEXEC') or die;

/**
 * @author Sapaleks
 */
class SenderModelRequests extends JModelList
{

	/**
	 * Конструктор класса
	 * @param Array $config
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array('id', 'date', 'name', 'phone', 'inn');
		}
		parent::__construct($config);
	}

	/**
	 * @param String $ordering
	 * @param String $direction
	 */
	protected function populateState($ordering = 'id', $direction = 'desc')
	{
		if ($layout = JFactory::getApplication()->input->get('layout')) {
			$this->context .= '.' . $layout;
		}
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);
		$authorId = $this->getUserStateFromRequest($this->context . '.filter.author_id', 'filter_author_id');
		$this->setState('filter.author_id', $authorId);
		parent::populateState($ordering, $direction);
	}

	/**
	 * @param string $id
	 * @return string
	 */
	protected function getStoreId($id = '')
	{
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.published');
		$id .= ':' . $this->getState('filter.author_id');
		return parent::getStoreId($id);
	}

	/**
	 * Overrides the getItems method to attach additional metrics to the list.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */
	public function getItems()
	{
		// Get a storage key.
		$store = $this->getStoreId('getItems');

		// Try to load the data from internal storage.
		if (!empty($this->cache[$store]))
		{
			return $this->cache[$store];
		}

		// Load the list items.
		$items = parent::getItems();

		// If emtpy or an error, just return.
		if (empty($items))
		{
			return array();
		}

		// Getting the following metric by joins is WAY TOO SLOW.
		// Faster to do three queries for very large menu trees.

		$db = $this->getDbo();



		foreach ($items as $item)
		{
			// Get the admitad results.
			$query = $db->getQuery(true)
				->select('ad.*, c.name')
				->from('#__admitad_results AS ad')
				->join(
					'LEFT',
					'#__admitad_campaigns AS c ON ad.campaign_id = c.id'
				)->where('ad.request_id = ' . $item->id);

			$db->setQuery($query);

			try
			{
				$item->admitad = $db->loadObjectList('campaign_id');
			}
			catch (RuntimeException $e)
			{
				$this->setError($e->getMessage());

				return false;
			}


		}

		// Add the items to the internal cache.
		$this->cache[$store] = $items;

		return $this->cache[$store];
	}

	/**
	 * Составление запроса для получения списка записей
	 * @return JDatabaseQuery
	 */
	protected function getListQuery()
	{
		$query = $this->getDbo()->getQuery(true);
		$query->select('r.*');
		//$query->select( 'u.username as created_by, u.id as author_id' );
		//$query->join( 'LEFT', '#__admitad AS ad ON u.id = t1.created_by' );
		$query->from('#__requests as r');
		/*$query->select('ad.*')->join(
			'LEFT',
			'#__admitad_results AS ad ON ad.request_id = r.id'
		);
		/*$published = $this->getState( 'filter.published' );
		if ( is_numeric( $published ) ) {
			$query->where( 't1.published=' . (int)$published );
		}
		$authorId = $this->getState( 'filter.author_id' );
		if ( is_numeric( $authorId ) ) {
			$query->where( 'u.id=' . $authorId );
		}*/
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $this->getDbo()->Quote('%' . $this->getDbo()->escape($search, true) . '%');
			$query->where('(r.name LIKE ' . $search . ' OR r.phone LIKE ' . $search . ' OR r.inn LIKE ' . $search . ')');
		}
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		$query->order($this->getDbo()->escape($orderCol . ' ' . $orderDirn));
		return $query;
	}

	/**
	 * Авторы записей
	 * @return    JDatabaseQuery
	 */
	public function getAuthors()
	{
		$query = $this->getDbo()->getQuery(true);
		$query->select('u.id AS value, u.name AS text');
		$query->from('#__users AS u');
		$query->join('INNER', '#__requests AS c ON c.created_by = u.id');
		$query->group('u.id, u.name');
		$query->order('u.name');
		return $this->getDbo()->setQuery($query)->loadObjectList();
	}
}