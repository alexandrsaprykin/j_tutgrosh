CREATE TABLE IF NOT EXISTS `#__campaigns` (
	`id` int(11) NOT NULL auto_increment, 
	`campaign_id` INT(11) NOT NULL, 
	`name` VARCHAR(50) NOT NULL, 
	`method_name` VARCHAR(50) NOT NULL, 
	`curl_count` INT(11) NOT NULL, 
	`ad_count` INT(11) NOT NULL, 
	`day_limit` INT(11) NOT NULL DEFAULT 0, 
	UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;
CREATE TABLE IF NOT EXISTS `#__requests` (
  `id` int(11) NOT NULL auto_increment, 
  `date` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `phone2` varchar(50) NOT NULL,
  `inn` varchar(50) NOT NULL,
  `age` varchar(255) NOT NULL,
  `obl` varchar(255) NOT NULL,
  `tip` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `tip2` varchar(50) NOT NULL,
  `sum` varchar(255) NOT NULL,
  `referer` varchar(255) NOT NULL,
  `istok` varchar(255) NOT NULL,
  `device` varchar(255) NOT NULL,
  `alfa_result` varchar(255) NOT NULL,
  `confirm_result` varchar(255) NOT NULL,
  `sended` smallint(6) NOT NULL DEFAULT '0',
  `log` varchar(255) NOT NULL,
  `credit_type` varchar(50) NOT NULL,
  `confirm_result_log` smallint(6) NOT NULL DEFAULT '0',
	UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;
CREATE TABLE IF NOT EXISTS `#__tokens` (
	`id` int(11) NOT NULL auto_increment, 
	`api_name` varchar(255) NOT NULL,
	`token` varchar(255) NOT NULL,
	`refresh_token` varchar(255) NOT NULL ,
	UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;
