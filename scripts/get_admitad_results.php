<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 19.12.2018
 * Time: 17:47
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
	require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', preg_replace('/(\\\|\/)scripts$/', '', dirname(__FILE__)));
	require_once JPATH_BASE . '/includes/defines.php';
}
define('JPATH_COMPONENT', JPATH_BASE . '/components/com_sender');

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_BASE . '/includes/framework.php';

require_once JPATH_COMPONENT . '/helpers/sender.php';

$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

$db = JFactory::getDbo();

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

JModelLegacy::addIncludePath(JPATH_COMPONENT . '/sending', 'SendingModel');

senderSiteHelper::initLogger();

$ad_model = JModelLegacy::getInstance('Admitad', 'SendingModel');
$com_sender = JComponentHelper::getComponent('com_sender');
$params = new JRegistry($com_sender->getParams());
$ad_id = $params->get('admitad_id');


$ad_model->start($ad_id);

$orders = $ad_model->getNewAdmitadRequests();

foreach ($orders as $key => $order) {

	$list = $ad_model->getRequestResult($order['admitad_id']);
	//var_dump($list);

	foreach ($list['results'] as $num => $result) {
		//var_dump($result);
		switch ($result["responses"][0]["status"]) {
			case 'approved':
				$result_id = 1;
				break;
			case 'declined':
				$result_id = 2;
				break;
			case 'error':
				$result_id = 3;
				break;
			default: //todo проверить статус waiting - изменяется в итоге или нет
				$result_id = 0;
				break;
		}
		$text = json_encode($result["responses"][0]["server_response"], JSON_UNESCAPED_UNICODE);
		$data = [
			'result' => $result_id,
			'result_text' => $text,
		];
		$ad_model->dbResultWriter($order['admitad_id'], $data);
		var_dump($result_id, $text);
		echo '<br>';
		echo '---------------------------';
		echo '<br>';
	}
}

