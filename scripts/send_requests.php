<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 21.11.2018
 * Time: 20:45
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
//define("path", "/home/credit24org/public_html/");
define("path", "");
define("sending_debug", true);
define("sending_log_file", path . "sending.log");

include path . 'config.php';
include_once path . 'libs/DB_functions.php';

if (!function_exists('to_log')) {
	function to_log($str)
	{

		if (sending_debug) {
			$f = fopen(sending_log_file, "a+");
			fputs($f, $str . ' - ' . date("Y-m-d H:i:s") . "\r\n");
			fclose($f);
		}
		return false;
	}
}

$db = new DBFunctions(DB_NAME, DB_HOST, DB_USER, DB_PASS);

$from = strtotime("14.11.2018 11:00"); //дата и время с которого начать отправлять заявки
$to = strtotime("14.11.2018 16:20");  //дата и время до которого  отправлять заявки
//$to = time();


//отправка заявок
$all_data = array();
$all_data = $db->getNotSendedByTime($from, $to);

foreach ($all_data as $key => $data) {

	$data2 = array(
		'sended' => 1,
	);
	$db->update($data2, $data['id']);// ToDO при неудачной отпрвке - 0 в базу

	to_log('----------------------------');
	to_log('Обработка заявки от ' . $data['name'] . ' (' . $data['phone'] . ') (' . $data['phone2'] . ') ИНН: ' . $data['inn']);
	to_log('отправка в Адмитад-Альфа. Результат: ' . $data['alfa_result']);
	if (!$data['confirm_result_log'] and $data['confirm_result']) {
		to_log($data['confirm_result']);
		$data2 = array(
			'confirm_result_log' => 1,
		);
		$db->update($data2, $data['id']);
	}
echo $data['name'] . '<br>';
	$send = true;
	$vsi_ok = true;
	if ($data['obl'] != 'Автономная Республика Крым' and $data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область' and $data['obl'] != 'Черновицкая область' and $data['obl'] != 'Волынская область') {
		if (($data['tip2'] == "NeR" or $data['tip2'] == "RN" or $data['tip2'] == "VS" or $data['tip2'] == "m3m" or $data['tip2'] == "CPm1g")) {
			$vsi_ok = false;
			$send = true;
		} else {
			if (stristr($data['alfa_result'], "Отказано")) {
				if ($data['age'] >= 27 and $data['age'] != "до 21 года" and (!is_numeric($data['sum']) || $data['sum'] >= 20000)) {//условия передачи при ответе "отказано"
					$vsi_ok = true;
					$send = false;
				} else {
					$vsi_ok = false;
					$send = true;
				}
			} else {
				if ($data['age'] >= 27 and $data['age'] != "до 21 года" and (!is_numeric($data['sum']) || $data['sum'] >= 20000)) {//условия передачи при ответе "Успешно" и др.
					$vsi_ok = true;
					$send = false;
				} else {
					$vsi_ok = false;
					$send = true;
				}
			}
		}
	} else {
		$vsi_ok = false;
		$send = true;
	}
	if ($send) {
		include path . 'admitad_sending.php';
	} else {
		to_log('Заявка не прошла условие отправки в остальные банки');
	}

}
