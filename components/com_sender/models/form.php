<?php

// No direct access
defined('_JEXEC') or die;

/**
 * Model for edit/create current element
 * @author Sapaleks
 */
class SenderModelForm extends JModelAdmin
{
	/**
	 * Method of loading the current form
	 * @param Array $data
	 * @param Boolean $loadData
	 * @return Object form data
	 */
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('', 'form', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}

	/**
	 * Method of loading table for current item
	 * @param Sting $type (name table)
	 * @param String $prefix (prefix table)
	 * @param Array $config
	 * @return Table
	 */
	public function getTable($type = 'requests', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method of loading data to form
	 * @return Object
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_sender.edit.form.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}
		return $data;
	}

	function to_log($path = '/sending.log', $str)
	{
		$f = fopen($path, "a+");
		fputs($f, $str . ' - ' . date("Y-m-d H:i:s") . "\r\n");
		fclose($f);
		return false;
	}


	public function allowSave($data)
	{
		return true;
	}

	/**
	 * save data
	 * @param array $data
	 * @return bool
	 */
	public function saveRequest($data)
	{
		$redirect_url = false;
		$params = JComponentHelper::getParams('com_sender');
		$alfa_id = $params->get('alfa_id');
		$data = $this->modify_data($data);
		$alfa_result = senderSiteHelper::admitadAlfaSend($data, $alfa_id);

		$table = $this->getTable();
		$save_data = [
			'date' => time(),
			'name' => $data['contact_name'],
			'phone' => $data['phone'],
			'phone2' => $data['phone2'],
			'inn' => $data['idencod'],
			'age' => $data['age'],
			'obl' => $data['obl'],
			'tip' => $data['tip'],
			'duration' => $data['mail_tip'],
			'tip2' => $data['tip2'],
			'sum' => $data['summa'],
			'referer' => $data['referer'],
			'istok' => $data['istok'],
			'device' => $data['device'],
			'alfa_result' => $alfa_result['result_text'],
			'credit_type' => $data['type'],
		];

		//Заносим данные в таблицу
		$table->bind($save_data);
		$table->store();

		//$alfa_result['out']['status'] = 'sended sms';

		//Редирект на странцу подтверждения
		if ($alfa_result['out']['status'] == 'sended sms') {
			$redirect_url = JRoute::_(
				'/?page=confirm_page&id=' . $alfa_result['out']['id'] . '&sms_id=' . $alfa_result['out']['sms_id']
				. '&db_id=' . $table->id . '&contact_name=' . $data['contact_name']
			);
		}else{
			$redirect_url = JRoute::_('/?page=order&ok=' . $data['ok']);
		}

		return $redirect_url;
	}

	public function checkSMSCode($data)
	{
		$urlApi = 'https://alfa-credits.com.ua/sms_submit/';
		$send_data = array(
			"sms_id" => $data['sms_id'],
			"sms_code" => $data['code'],
			"id" => $data['id'],
		);
		$request = $urlApi . "?" . http_build_query($send_data);
		$err = false;

		$out = json_decode(file_get_contents($request), true);
		if(!$out){
			$curl = curl_init();
			if ($curl) {
				curl_setopt($curl, CURLOPT_URL, $request);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				$out = curl_exec($curl);
				curl_close($curl);
			}
			$out = json_decode($out, true);
		}

//$out['status'] = 'error';
//$out['status'] = 'success';
//$out['id'] = 1;
		if ($out['status'] == 'success' && $out['id'] != 0) {
			$str = 'Заявка от ' .  $_POST['name'] . ' ПРОШЛА СМС-ВЕРИФИКАЦИЮ УСПЕШНО!!!-----------@@@@@@@@@@@@@@@@@@@@@@@@@@';
		}elseif ($out['status'] == 'error'){
			$str = 'Заявка от ' .  $data['name'] . '. Ошибка при отправке. Текст: ' . (is_array($out['error'])? urldecode(http_build_query($out['error'])) : $out['error']);
			$str .= ' sms_id=' . $data['sms_id'] . ' sms_code='. $data['code'] . ' id=' . $data['id'];
			$err = true;
		}elseif ($out['status'] == 'success' && $out['id'] == 0){
			$str = 'Заявка от ' .  $data['name'] . '. Результат: Подтверждение смс - ОТКАЗАНО!!!-----------@@@@@!!!!!!!!!!!@@@@@!!!!!!!!!!!!@@@@' ;
		}elseif(!$out){
			$str = 'Заявка от ' .  $data['name'] . '. Пустой ответ от сервера.@@@@@@@!!!!!!!@@@@@@@@!!!!!!!! ';
			$err = true;
		}else{
			$str = 'Заявка от ' .  $data['name'] . ' не прошла СМС-верификацию.@@@@@@@!!!!!!!@@@@@@@@!!!!!!!! Ответ: ' . $out ;
			$err = true;
		}
		$db = $this->getDbo();

		$save_data = array(
			'confirm_result=' . $db->quote($str),
		);
		$conditions = array(
			'id=' . $data['db_id'],
		);
		$query = $db->getQuery(true)
			->update('#__requests')
			->set($save_data)
			->where($conditions);

		$db->setQuery($query);
		$db->execute();

		if($err){
			return json_encode( array('success' => false));
		}else{
			return json_encode( array('success' => true));
		}

	}


	public function modify_data($data)
	{
		$data['contact_name'] = (string)$data['contact_name']; // преобразуем в строковое значение
		$data['contact_name'] = strip_tags($data['contact_name']); // убираем HTML-теги
		$data['contact_name'] = preg_replace('/\s+/', ' ', $data['contact_name']); // удаляем повторяющие пробелы
		$data['contact_name'] = trim($data['contact_name']); // убираем пробелы в начале и конце строки
		$data['contact_name'] = str_replace("i", "і", $data['contact_name']); // заменяем англ на укр
		$data['contact_name'] = preg_replace("/[\'\’\-\.«»\"\']/ui", "", $data['contact_name']); // очищаем строку от недопустимых символов

		$data['istok'] = str_replace('-', '', $data['istok']);
		if ($data['istok'] == 'Sourcenotdefined') {
			if (strpos($data['referer'], 'google') > 0) {
				$data['istok'] = 'Google';
			} elseif (strpos($data['referer'], 'yandex') > 0) {
				$data['istok'] = 'Yandex';
			}
		}

		//анализ трудоустройства
		$data['mail_tip'] = 0;
		$data['tip2'] = 0;
		if ($data['tip'] == "Я военнослужащий") {
			//$data['tip'] = 'Я работаю неофициально';
			$data['tip2'] = "VS";
		}
		if ($data['tip'] == "Я получаю пенсию") {
			$data['tip2'] = "P";
		}
		if ($data['tip'] == "Я работаю неофициально") {
			$data['tip2'] = "RN";
		}
		if ($data['tip'] == "Я не работаю") {
			$data['tip2'] = "NeR";
		}
		if ($data['tip'] == "Я работаю официально") {
			if ($data['work_duration'] == 'официально меньше 3') {
				$data['tip'] = 'Я работаю неофициально';
				$data['mail_tip'] = "Я работаю официально меньше 3 мес.";//для письма
				$data['tip2'] = "m3m";
			} else {
				$data['mail_tip'] = "Я работаю официально больше 3 мес.";//для письма
				$data['tip2'] = "B3m";
			}
		}
		if ($data['tip'] == "Я частный предприниматель") {
			if ($data['reg_duration'] == 'меньше 1 года') {
				$data['tip'] = 'Я работаю неофициально';
				$data['mail_tip'] = "Я частный предприниматель меньше 1 года";//для письма
				$data['tip2'] = "CPm1g";
			} else {
				$data['mail_tip'] = "Я частный предприниматель больше 1 года";//для письма
				$data['tip2'] = "CPB1g";
			}
		}

		return $data; // возвращаем результат
	}

}