<?php
// No direct access
defined('_JEXEC') or die;

/**
 * Controller
 * @author Sapaleks
 */
class SenderControllerForm extends JControllerForm
{

	/**
	 * Class constructor
	 * @param array $config
	 */
	function __construct($config = array())
	{
		$this->view_list = 'Form';
		parent::__construct($config);
	}

	/**
	 * @return bool
	 */

	public function smsConfirm()
	{
		$data = $this->input->post->getArray();
		//die(json_encode($data));
		$model = $this->getModel();
		//получаем нашу форму
		$result = $model->checkSMSCode($data);

		die($result);

	}

	public function mail()
	{
		$site_name = JFactory::getConfig()->get('sitename');
		$site_addr = JFactory::getConfig()->get('site_addr');
		$data = $this->input->post->getArray();
		$msg = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>";
		foreach ($data as $key => $value) {
			$msg = $msg . '<b>' . $key . ': </b>' . $value . '<br>';
		}
		$msg = $msg . '</body></html>';
		$headers = "Content-Type: text/html; charset=utf-8\r\nFrom: " . $site_addr . " <form@tutgroshi.com.ua>\r\n";

		mail('vsi.ok.zayavka@gmail.com', $data['type'] . " " . $site_name, $msg, $headers);
		mail('vsi.ok.zayavka@yandex.ua', $data['type'] . " " . $site_name, $msg, $headers);
	}

	public function newRequest()
	{
		//Получаем наше приложение
		$app = JFactory::getApplication();
		//Данные коотрые пришли из формы
		$data = $this->input->post->getArray();
		/*$data = array(
			'date' => '1537779399',
			'contact_name' => 'Джумла Тест',
			'phone' => '0990000000',
			'phone2' => '',
			'idencod' => '1111111111',
			'age' => '26',
			'obl' => 'Житомирская область',
			'tip' => 'Я получаю пенсию',
			'duration' => '0',
			'tip2' => '',
			'summa' => '20000',
			'referer' => 'другой источник\r\n',
			'istok' => 'Sourcenotdefined',
			'device' => 'Компьютер',
			'alfa_result' => 'Успешно',
			'confirm_result' => 'Заявка от Тест Тест ПРОШЛА СМС-ВЕРИФИКАЦИЮ УСПЕШНО!!!-----------@@@@@@@@@@@@@@@@@@@@@@@@@@',
			'sended' => 1,
			'log' => '',
			'credit_type' => '',
			'confirm_result_log' => 1
		);*/
		//Получаем нашу модель

		if ($data['contact_name']) {
			$model = $this->getModel();

			$url = $model->saveRequest($data);

			if ($url) {
				$this->setRedirect($url);
			}
		}

		return 0;
	}

}