<?php

// No direct access
defined('_JEXEC') or die;

include_once JPATH_COMPONENT . '/sending/admitad_api.php';

/**
 *
 * @author Sapaleks
 */
class SendingModelAdmitad extends SendingModelAdmitadApi
{
	public function start($site_id, $script = false)
	{
		$this->site_id = $site_id;
		$this->campaigns = $this->getAdmitadCampaigns();
		$this->script = $script;
		$this->getToken();
		$this->checkToken();
	}

	public function send($data, $site_id, $developer_mode = false, $script = false)
	{
		if (!$this->site_id) {
			$this->start($site_id, $script);
		}

		$name = explode(" ", $data['name']);
		$lastName = (isset($name[0]) ? $name[0] : $data['name']);
		$firstName = (isset($name[1]) ? $name[1] : "");
		$middleName = (isset($name[2]) ? $name[2] : "Отчество");

		$number = substr($data['inn'], 0, 5);
		$b_date = date('d.m.Y', strtotime('01.01.1900 + ' . $number . ' days - 1 days'));

		switch ($data['tip']) {
			case "Я работаю официально":
			case "Я работаю неофициально":
			case "Я военнослужащий":
			case "Я не работаю":
				$employment = "worker";
				break;

			case "Я частный предприниматель":
				$employment = "entrepreneur ";
				break;

			case "Я получаю пенсию":
				$employment = "pensioner";
				break;
			default:
				$employment = "worker";
				break;

		}

		switch ($data['obl']) {
			case "Киевская область":
				$region = "Київ";
				break;
			case "Днепропетровская область":
				$region = "Дніпро";
				break;
			case "Винницкая область":
				$region = "Вінниця";
				break;
			case "Запорожская область":
				$region = "Запоріжжя";
				break;
			case "Кировоградская область":
				$region = "Кропивницький";
				break;
			case "Львовская область":
				$region = "Львів";
				break;
			case "Николаевская область":
				$region = "Миколаїв";
				break;
			case "Одесская область":
				$region = "Одеса";
				break;
			case "Полтавская область":
				$region = "Полтава";
				break;
			case "Ровненская область":
				$region = "Рівне";
				break;
			case "Черкаська область":
				$region = "Черкаси";
				break;
			case "Тернопольская область":
				$region = "Тернопіль";
				break;
			case "Харьковская область":
				$region = "Харків";
				break;
			case "Херсонская область":
				$region = "Херсон";
				break;
			default:
				$region = 0;
				break;
		}

		$data2 = array(
			'test_mode' => ($developer_mode ? 1 : 0),
			'last_name' => $lastName,
			'first_name' => $firstName,
			'middle_name' => $middleName,
			'mobile_phone' => $data['phone'],
			'inn' => $data['inn'],
			'credit_sum' => (int)$data['sum'],
			'birth_date' => $b_date,
			'subid' => $data['istok'],
			'subid1' => $data['phone'],
			'subid2' => $data['age'],
			'subid3' => (int)$data['sum'],
			'subid4' => $data['inn'],
			'product_type' => 'card', //Monobank
			//'product_type' => 'Кредит готівкою', //ПУМБ
			//'product_type' => 1, //PrivatBank
			'email' => 'litekredit@gmail.com',
		);

		$info = [
			'name' => $data['name'],
			'tip' => $data['tip'],
			'tip2' => $data['tip2'],
			'region' => $region,
			'occupation' => $employment,
			'obl' => $data['obl'],
			'age' => $data['age'],
			'script' => (isset($data['script']) ? 1 : 0),
		];

		foreach ($this->campaigns as $campaign) {

			$info['curl_count'] = $campaign['curl_count'];
			$info['ad_count'] = $campaign['ad_count'];
			$info['day_limit'] = $campaign['day_limit'];
			$info['campaign_id'] = $campaign['id'];
			$info['request_id'] = $data['id'];

			//if(!$campaign['day_limit']){
			$data2['campaigns'] = array((int)$campaign['campaign_id']);
			call_user_func_array(array($this, 'send' . $campaign['method_name']), [$info, $data2]);
			$this->incrementAd($campaign['campaign_id'], $campaign['ad_count']);
			//}

			if ($campaign['day_limit'] && method_exists($this, 'curl' . $campaign['method_name']) /*&& $campaign['curl_count'] < 60*/) {
				$result = call_user_func_array(array($this, 'curl' . $campaign['method_name']), [$info, $data2]);
				$this->incrementCurl($campaign['campaign_id'], $campaign['curl_count']);
				//$ad->to_log('Отправка вручную в ' . $campaign['name'] . '. Результат: ' . $result);
				$this->to_log('Отправка на спец почту по ' . $campaign['name']);
			}


		}
	}

	public function sendCreditDnepr($info, $data)
	{

		//Кредит Днепр
		if ($info['tip2'] != "VS" and
			$info['obl'] != 'Автономная Республика Крым' and $info['obl'] != 'Луганская область' and $info['obl'] != 'Донецкая область' and
			$info['obl'] != 'Черновицкая область' and $info['obl'] != 'Ивано-Франковская область' and $info['obl'] != 'Николаевская область' and
			$info['obl'] != 'Черниговская область' and $info['obl'] != 'Волынская область' and $info['obl'] != 'Закарпатская область' and
			$info['obl'] != 'Тернопольская область'
		) {
			if ($info['age'] > 21 and $info['age'] != "до 21 года" and $info['age'] != "больше 70" and $info['age'] <= 65) {
				$this->to_log('Admitad. Отправка в Кредит Днепр(' . $data['last_name'] . '):');
				$respond = $this->sendRequest($data);
				$this->resultWriter($respond, $info);
				$this->checkLimit($data['campaigns'][0], $respond, $info['day_limit']);
			} else {
				$this->to_log('Admitad. Отправка в Кредит Днепр(' . $data['last_name'] . '): Не удовлетворяет по возрасту');
			}
		} else {
			$this->to_log('Admitad. Отправка в Кредит Днепр(' . $data['last_name'] . '): Не удовлетворяет по труду или области');
		}


	}

	public function sendGroshi($info, $data)
	{
		//Швидко Гроші
		$this->to_log('Admitad. Отправка в Швидко Гроші:');
		$respond = $this->sendRequest($data);
		$this->resultWriter($respond, $info);
		// $this->checkLimit($data['campaigns'][0], $respond);
	}

	public function sendCashpoint($info, $data)
	{
		//Cashpoint
		if($data['credit_sum'] > 7000) {
			$data['credit_sum'] = 7000;
		}

		$this->to_log('Admitad. Отправка в Cashpoint:');
		$respond = $this->sendRequest($data);
		$this->resultWriter($respond, $info);
		// $this->checkLimit($data['campaigns'][0], $respond);
	}

	public function sendMonobank($info, $data)
	{
		//Monobank
		$this->to_log('Admitad. Отправка в Monobank:');
		$respond = $this->sendRequest($data);
		$this->resultWriter($respond, $info);
		// $this->checkLimit($data['campaigns'][0], $respond);
	}

	public function sendPumb($info, $data)
	{
		//Pumb
		if ($info['age'] != "до 21 года" and $info['age'] >= 25) {
			$this->to_log('Admitad. Отправка в ПУМБ:');
			$data['product_type'] = 'Кредитна картка ВСЕМОЖУ';
			$respond = $this->sendRequest($data);
			$this->resultWriter($respond, $info);
			$this->checkLimit($data['campaigns'][0], $respond, $info['day_limit']);
		}
	}

	public function sendPrivatBank($info, $data)
	{
		//PrivatBank
		$this->to_log('Admitad. Отправка в PrivatBank:');
		$data['product_type'] = 7;
		$respond = $this->sendRequest($data);
		$this->resultWriter($respond, $info);
		// $this->checkLimit($data['campaigns'][0], $respond);
	}

	public function sendUnex($info, $data)
	{
		//Unex

		$data['fact_city_name'] = $info['region'];
		$data['credit_days'] = 36;
		$data['occupation'] = $info['occupation'];
		$data['credit_purpose'] = 'repairs';
		if ($data['occupation'] && $info['tip'] != "Я не работаю" && $info['tip'] != "Я работаю неофициально") {
			$this->to_log('Admitad. Отправка в Unex:');
			$respond = $this->sendRequest($data);
			$this->resultWriter($respond, $info);
		}
		//$this->checkLimit($data['campaigns'][0], $respond);
	}

	public function sendEcredit($info, $data)
	{
		//Ecredit
		$this->to_log('Admitad. Отправка в Ecredit:');
		$respond = $this->sendRequest($data);
		$this->resultWriter($respond, $info);
		//$this->checkLimit($data['campaigns'][0], $respond);
	}

	public function sendIdeabank($info, $data)
	{
		$data['product_type'] = 2;
		if ($info['age'] != "до 21 года" and $info['age'] >= 30 and $info['age'] != "больше 70" and $info['age'] <= 65 and $info['tip2'] != "VS" and
			$data['credit_sum'] >= 10000 and $data['credit_sum'] < 20000 and ($info['tip2'] == "B3m" or $info['tip2'] == "CPB1g" or $info['tip2'] == "P") and !$info['script']
		) {
			$this->to_log('Admitad. Отправка в Ideabank:');
			$respond = $this->sendRequest($data);
			$this->resultWriter($respond, $info);
			//$this->checkLimit($data['campaigns'][0], $respond);
		} else {
			$this->to_log('Admitad. Отправка в Ideabank(' . $data['last_name'] . '): Не удовлетворяет по условиям');
		}
	}


	public function curlCreditDnepr($info, $data)
	{
		$headers = "Content-Type: text/html; charset=utf-8\r\nFrom: credit24.org.ua <form@credit24.org.ua>\r\n";
		$msg = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
	<br><b>ФИО:</b> " . $info['name'] . "<br>
	<b>Телефон:</b> " . $data['mobile_phone'] . "<br>
	<b>Идентификационный код:</b> " . $data['inn'] . "<br>
	<b>Возраст:</b> " . $info['age'] . "<br>
	<b>Область фактического проживания:</b> " . $info['obl'] . "<br>
	<b>Тип дохода:</b> " . $info['tip'] . "<br>
	<b>Сумма кредита:</b> " . $data['credit_sum'] . " грн.<br>
	<b>Источник заявки:</b> " . $data['subid'] . "<br>
	</body></html>";

		if ($info['tip2'] != "VS" and
			$info['obl'] != 'Автономная Республика Крым' and $info['obl'] != 'Луганская область' and $info['obl'] != 'Донецкая область' and
			$info['obl'] != 'Черновицкая область' and $info['obl'] != 'Ивано-Франковская область' and $info['obl'] != 'Николаевская область' and
			$info['obl'] != 'Черниговская область' and $info['obl'] != 'Волынская область' and $info['obl'] != 'Закарпатская область' and
			$info['obl'] != 'Тернопольская область'
		) {

			if ($info['age'] > 21 and $info['age'] != "до 21 года" and $info['age'] != "больше 70" and $info['age'] <= 65) {
				if (mail('kreditdnipro.zayavka@gmail.com', "заявка Кредит24 после лимита", $msg, $headers)) { //mail('superposition2008@gmail.com', "заявка Кредит24", $allmsg, $headers);
					//$log .= 'отправлено на почту.' . ' ';
				}
			}

		}
	}

	public function curlPumb($info, $data)
	{
		$headers = "Content-Type: text/html; charset=utf-8\r\nFrom: credit24.org.ua <form@credit24.org.ua>\r\n";
		$msg = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
	<br><b>ФИО:</b> " . $info['name'] . "<br>
	<b>Телефон:</b> " . $data['mobile_phone'] . "<br>
	<b>Идентификационный код:</b> " . $data['inn'] . "<br>
	<b>Возраст:</b> " . $info['age'] . "<br>
	<b>Область фактического проживания:</b> " . $info['obl'] . "<br>
	<b>Тип дохода:</b> " . $info['tip'] . "<br>
	<b>Сумма кредита:</b> " . $data['credit_sum'] . " грн.<br>
	<b>Источник заявки:</b> " . $data['subid'] . "<br>
	</body></html>";

		if ($info['age'] > 25 and $info['age'] != "до 21 года") {

			if (mail('pumb.zayavka@gmail.com', "заявка Кредит24 после лимита", $msg, $headers)) { //mail('superposition2008@gmail.com', "заявка Кредит24", $allmsg, $headers);
				//$log .= 'отправлено на почту.' . ' ';
			}
		}
	}


}