<?php

// No direct access
defined('_JEXEC') or die;

/**
 * Model for edit/create current element
 * @author Sapaleks
 */
class SendingModelMail extends JModelList
{
	protected $script = false;

	function start($script = false)
	{
		$this->script = $script;
	}

	function sending_idea($data)
	{
		if (!($data['age'] != "до 21 года" and $data['age'] >= 30 and $data['age'] != "больше 70" and $data['age'] <= 65 and $data['tip2'] != "VS" and
			$data['sum'] >= 10000 and $data['sum'] < 20000 and ($data['tip2'] == "B3m" or $data['tip2'] == "CPB1g" or $data['tip2'] == "P"))
		) {
			require_once('lib/nusoap.php');
			$client = new nusoap_client("https://194.126.180.186:77/ScroogeCbForms.svc?wsdl", 'wsdl');
			$client->soap_defencoding = 'UTF-8';

			$params = '
                <CreateOrderBroker xmlns="http://tempuri.org/">
                <shortApp xmlns:a="http://schemas.datacontract.org/2004/07/ScroogeCbformsService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                <a:PIB>' . $data['name'] . '</a:PIB>
                <a:agreeId>3544</a:agreeId>
                <a:formId>92</a:formId>
                <a:stateCode>' . $data['inn'] . '</a:stateCode>
                <a:telephone>' . $data['phone'] . '</a:telephone>
                </shortApp>
                </CreateOrderBroker>
                ';

			$result = $client->call('CreateOrderBroker', $params);
			if (isset($result['CreateOrderBrokerResult']['ReturnValue']) and isset($result['CreateOrderBrokerResult']['OrderId'])) {
				$idea = 'отправлено в Ідеябанк, ответ сервера - id' . $result['CreateOrderBrokerResult']['OrderId'] . '(' . $result['CreateOrderBrokerResult']['ReturnValue'] . ')';
				$this->to_log('отправлено в Ідеябанк напрямую, ответ сервера - id' . $result['CreateOrderBrokerResult']['OrderId'] . '(' . $result['CreateOrderBrokerResult']['ReturnValue'] . ')');
			} else {
				$idea = 'проблемы отправки в Ідеябанк';
				$this->to_log('проблемы отправки в Ідеябанк напрямую, ответ сервера - ' . $result['CreateOrderBrokerResult']['ReturnValue']);
			}

			/*
			Результати обробки заявки
			99 – за добу заявка з ІПН вже заведена
			100 – не вірний ІПН
			101 – клієнту менше 21-го року
			102 – заборонена видачі по іншим причинам
			1 - помилка системи
			0 – заявка заведена успішно.
			*/
		}

	}

	function sending_script_idea($data)
	{

		require_once('lib/nusoap.php');
		$client = new nusoap_client("https://194.126.180.186:77/ScroogeCbForms.svc?wsdl", 'wsdl');
		$client->soap_defencoding = 'UTF-8';

		$params = '
                <CreateOrderBroker xmlns="http://tempuri.org/">
                <shortApp xmlns:a="http://schemas.datacontract.org/2004/07/ScroogeCbformsService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                <a:PIB>' . $data['name'] . '</a:PIB>
                <a:agreeId>3544</a:agreeId>
                <a:formId>91</a:formId>
                <a:stateCode>' . $data['inn'] . '</a:stateCode>
                <a:telephone>' . $data['phone'] . '</a:telephone>
                </shortApp>
                </CreateOrderBroker>
                ';

		$result = $client->call('CreateOrderBroker', $params);
		if (isset($result['CreateOrderBrokerResult']['ReturnValue']) and isset($result['CreateOrderBrokerResult']['OrderId'])) {
			$idea = 'отправлено в Ідеябанк, ответ сервера - id' . $result['CreateOrderBrokerResult']['OrderId'] . '(' . $result['CreateOrderBrokerResult']['ReturnValue'] . ')';
			$this->to_log('отправлено в Ідеябанк напрямую, ответ сервера - id' . $result['CreateOrderBrokerResult']['OrderId'] . '(' . $result['CreateOrderBrokerResult']['ReturnValue'] . ')');
		} else {
			$idea = 'проблемы отправки в Ідеябанк';

			$this->to_log(
				'проблемы отправки в Ідеябанк напрямую, ответ сервера - ' . $result['CreateOrderBrokerResult']['ReturnValue']
			);
		}

		/*
		Результати обробки заявки
		99 – за добу заявка з ІПН вже заведена
		100 – не вірний ІПН
		101 – клієнту менше 21-го року
		102 – заборонена видачі по іншим причинам
		1 - помилка системи
		0 – заявка заведена успішно.
		*/

		return $idea;
	}


	function sending_mega($data, $headers, $msg)
	{
		if ($data['age'] > 25 and $data['age'] != "до 21 года" and $data['tip2'] != "NeR" and $data['tip2'] != "RN" && $data['obl'] != 'Луганская область' && $data['obl'] != 'Черновицкая область' and $data['obl'] != 'Волынская область' &&
			$data['obl'] != 'Автономная Республика Крым' && $data['obl'] != 'Ровненская область' && $data['obl'] != 'Закарпатская область' && $data['obl'] != 'Одесская область'
		) {

			if (mail('mega.zayavka@gmail.com', 'заявка с Тутгроши', $msg, $headers)) {
				$this->to_log(
					'отправлено в Мегабанк'
				);
			} else {
				$this->to_log(
					'ошибка отправки в Мегабанк'
				);
			}
		}

	}

	function sending_aval($data, $headers, $msg)
	{

		if ($data['age'] > 23 and $data['age'] != "до 21 года" &&
			$data['obl'] != 'Автономная Республика Крым' && $data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область'
			and ($data['tip'] == "Я работаю официально" or $data['tip'] == "Я получаю пенсию")
		) {

			if (mail('aval.zayavka@gmail.com', 'заявка с Тутгроши', $msg, $headers)) {
				$this->to_log(
					'отправлено в Авальбанк'
				);
			} else {
				$this->to_log(
					'ошибка отправки в Авальбанк'
				);
			}
		}

	}

	function sending_kredo($data, $headers, $msg)
	{

		if ($data['age'] > 24 and $data['age'] != "до 21 года" &&
			$data['obl'] != 'Автономная Республика Крым' && $data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область'
			and ($data['tip'] == "Я работаю официально" or $data['tip'] == "Я получаю пенсию")
		) {

			if (mail('kredo.zayavka@gmail.com', 'заявка с Тутгроши', $msg, $headers)) {
				$this->to_log(
					'отправлено в Кредобанк'
				);
			} else {
				$this->to_log(
					'ошибка отправки в Кредобанк'
				);
			}
		}

	}

	function sending_agricol($data, $headers, $msg)
	{

		if ($data['age'] > 23 and $data['age'] != "до 21 года" &&
			$data['obl'] != 'Автономная Республика Крым' && $data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область'
			and ($data['tip'] == "Я работаю официально" or $data['tip'] == "Я получаю пенсию")
		) {

			if (mail('ca.zayavka@gmail.com', 'заявка с Тутгроши', $msg, $headers)) {
				$this->to_log(
					'отправлено в Кредиагрікольбанк'
				);
			} else {
				$this->to_log(
					'ошибка отправки в Кредиагрікольбанк'
				);
			}
		}

	}

	function sending_cash2go($data, $headers, $msg)
	{

		if ($data['age'] >= 23 and $data['age'] != "до 21 года" and $data['age'] != "больше 70" and $data['age'] <= 65 &&
			$data['obl'] == 'Киевская область' and (int)$data['sum'] >= 5000
		) {

			if (mail('sale@cash2go.com.ua', 'заявка с Тутгроши', $msg, $headers)) {
				$this->to_log(
					'отправлено в Cash2go'
				);
			} else {
				$this->to_log(
					'ошибка отправки в Cash2go'
				);
			}
		}

	}


	function sending_oksi($data, $headers, $msg)
	{
		if (mail('okcibank@ukr.net', 'заявка с Тутгроши', $msg, $headers)) {
			$this->to_log(
				'отправлено в Оксибанк'
			);
		} else {
			$this->to_log(
				'ошибка отправки в Оксибанк'
			);
		}

	}

	function sending_finline($data)
	{
		$err = false;


		$partner = "2829";
		$productType = "creditCash";
		$ip = "";
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}


		$phoneOperator = array("039", "050", "063", "066", "067", "068", "091", "092", "093", "094", "095", "096", "097", "098", "099", "073");


		$phone = $data['phone'];

		$phoneCode = substr($data['phone'], 0, 3);

		if (in_array($phoneCode, $phoneOperator)) {
			$phone = "38" . $phone;
		} else {
			$phone = "ErrorCode:" . $phone;
			$err = true;
		}

		$name = explode(" ", $data['name']);
		//$url

		$firstName = (isset($name[0]) ? $name[0] : $data['name']);
		//		$firstName= ( $data['contact_name']);
		$lastName = (isset($name[1]) ? $name[1] : "");
		//		$lastName="";
		$middleName = (isset($name[2]) ? $name[2] : "");
		//		$middleName="";
		$age = strip_tags(($data['age']));
		//$email="";
		$amount = $data['sum'];
		$amount = str_replace(' ', '', $amount);

		if (strpos($amount, '-')) $amount = substr($amount, 0, strpos($amount, '-'));
		if (strpos($amount, '.')) $amount = substr($amount, 0, strpos($amount, '.'));

		$employment = "";
		if ($data['tip'] == "Я не работаю") $employment = 'no';
		if ($data['tip'] == "Я частный предприниматель") $employment = 'private';
		if ($data['tip'] == "Я получаю пенсию") $employment = 'pensioner';
		if ($data['tip'] == "Я работаю официально") $employment = 'official';
		if ($data['tip'] == "Я работаю неофициально") $employment = 'unofficial';

		$aim = "renovation";
		$city = (($data['obl']));
		$channel = "TG";
		//$passport="";
		$identCode = strip_tags(($data['inn']));
		//$okCb="";
		$sinkCb = "http://tutgroshi.com.ua";
		$declineCb = "http://tutgroshi.com.ua";
		$istok = $data['istok'];

		$data2 = array("partner" => urlencode($partner),
			"productType" => urlencode($productType),
			"ip" => urlencode($ip),
			"phone" => urlencode($phone),
			"firstName" => ($firstName),
			"lastName" => ($lastName),
			"middleName" => ($middleName),
			"age" => urlencode($age),
			"amount" => urlencode($amount),
			"employment" => urlencode($employment),
			"aim" => urlencode($aim),
			"city" => ($city),
			"channel" => urlencode($channel),
			"identCode" => urlencode($identCode),
			"url" => urlencode($istok)
		);

		$urlApi = 'http://partner.finline.ua/api/apply/';
		$request = $urlApi . "?" . http_build_query($data2);
		$curl = curl_init();

		if ($curl) {
			curl_setopt($curl, CURLOPT_URL, $request);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$out = curl_exec($curl);
			curl_close($curl);
		}
		$out = json_decode($out, true);

		if ($out['result']) {
			$finline = 'Отправлено в Finline. Статус: ' . ($out['isDeclined'] ? 'Отклонена. Причина: ' . $out['declineReason'] : 'Принята');
			$this->to_log(
				'Отправлено в Finline. Статус: ' . ($out['isDeclined'] ? 'Отклонена. Причина: ' . $out['declineReason'] : 'Принята')
			);
		} else {
			$finline = 'Не отправлено в Finline. Ошибка: ' . $out['error'] . ' ' . $out['errorMessage'];
			$this->to_log(
				'Не отправлено в Finline. Ошибка: ' . $out['error'] . ' ' . $out['errorMessage']
			);
		}
		return $finline;

	}

	function sending_script_finline($data)
	{
		$err = false;

		$partner = "2829";
		$productType = "creditCash";
		$ip = "";
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		$phoneOperator = array("039", "050", "063", "066", "067", "068", "091", "092", "093", "094", "095", "096", "097", "098", "099", "073");

		$phone = $data['phone'];

		$phoneCode = substr($data['phone'], 0, 3);

		if (in_array($phoneCode, $phoneOperator)) {
			$phone = "38" . $phone;
		} else {
			$phone = "ErrorCode:" . $phone;
			$err = true;
		}

		$name = explode(" ", $data['name']);
		$firstName = (isset($name[0]) ? $name[0] : $data['name']);
		$lastName = (isset($name[1]) ? $name[1] : "");
		$middleName = (isset($name[2]) ? $name[2] : "");
		$age = strip_tags(($data['age']));
		$amount = $data['sum'];
		$amount = str_replace(' ', '', $amount);

		if (strpos($amount, '-')) $amount = substr($amount, 0, strpos($amount, '-'));
		if (strpos($amount, '.')) $amount = substr($amount, 0, strpos($amount, '.'));

		$employment = "";
		if ($data['tip'] == "Я не работаю") $employment = 'no';
		if ($data['tip'] == "Я частный предприниматель") $employment = 'private';
		if ($data['tip'] == "Я получаю пенсию") $employment = 'pensioner';
		if ($data['tip'] == "Я работаю официально") $employment = 'official';
		if ($data['tip'] == "Я работаю неофициально") $employment = 'unofficial';

		$aim = "renovation";
		$city = (($data['obl']));
		$channel = "TG";
		$identCode = strip_tags(($data['inn']));
		$sinkCb = "http://tutgroshi.com.ua";
		$declineCb = "http://tutgroshi.com.ua";
		$istok = $data['istok'];

		$data2 = array("partner" => urlencode($partner),
			"productType" => urlencode($productType),
			"ip" => urlencode($ip),
			"phone" => urlencode($phone),
			"firstName" => ($firstName),
			"lastName" => ($lastName),
			"middleName" => ($middleName),
			"age" => urlencode($age),
			"amount" => urlencode($amount),
			"employment" => urlencode($employment),
			"aim" => urlencode($aim),
			"city" => ($city),
			"channel" => urlencode($channel),
			"identCode" => urlencode($identCode),
			"url" => urlencode($istok)
		);

		$urlApi = 'http://partner.finline.ua/api/apply/';
		$request = $urlApi . "?" . http_build_query($data2);

		$curl = curl_init();

		if ($curl) {
			curl_setopt($curl, CURLOPT_URL, $request);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$out = curl_exec($curl);
			curl_close($curl);
		}
		$out = json_decode($out, true);

		if ($out['result']) {
			$finline = 'Отправлено в Finline. Статус: ' . ($out['isDeclined'] ? 'Отклонена. Причина: ' . $out['declineReason'] : 'Принята');
			$this->to_log(
				'Отправлено в Finline. Статус: ' . ($out['isDeclined'] ? 'Отклонена. Причина: ' . $out['declineReason'] : 'Принята')
			);
		} else {
			$finline = 'Не отправлено в Finline. Ошибка: ' . $out['error'] . ' ' . $out['errorMessage'];
			$this->to_log('Не отправлено в Finline. Ошибка: ' . $out['error'] . ' ' . $out['errorMessage']);
		}

		return $finline;
	}

	public function to_log($str)
	{
		if($this->script) {
			JLog::add($str, \JLog::DEBUG, 'script_sender');
		}else {
			JLog::add($str, \JLog::DEBUG, 'com_sender');
		}

		return false;
	}


}