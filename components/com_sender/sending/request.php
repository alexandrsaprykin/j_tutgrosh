<?php

// No direct access
defined('_JEXEC') or die;

/**
 * Model for edit/create current element
 * @author Sapaleks
 */
class SendingModelRequest extends JModelList
{

	public function writeRequest($data = array())
	{
		$keys = array_keys($data);
		foreach ($keys as $key) {
			$pdo_keys[] = ':' . $key;
			$values[':' . $key] = $data[$key];
		}
		$id = $this->add("INSERT INTO `#__requests`(" . implode($keys, ', ') . ") VALUES(" . implode($pdo_keys, ', ') . ")", $values);
		return $id;
	}

	public function writeScriptRequest($data)
	{
		$keys = array_keys($data);
		foreach ($keys as $key) {
			$values[$key] = '"' . $data[$key] . '"';
		}
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("INSERT INTO `#__script_requests`(" . implode($keys, ', ') . ") VALUES(" . implode($values, ', ') . ")");
		$db->setQuery($query);
		$id = $db->execute();
		return $id;
	}

	public function update($data = array(), $id)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$keys = array_keys($data);
		foreach ($keys as $key) {
			$parts[] = $key . '=' . $data[$key];
		}
		$query->setQuery("UPDATE `#__requests` SET " . implode($parts, ', ') . " WHERE id = " . $id);
		$db->setQuery($query);
		$db->execute();
	}

	public function scriptUpdate($data = array(), $id)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$keys = array_keys($data);
		foreach ($keys as $key) {
			$parts[] = $key . '=' . $data[$key];
		}
		$query->setQuery("UPDATE `#__script_requests` SET " . implode($parts, ', ') . " WHERE id = " . $id);
		$db->setQuery($query);
		$db->execute();
	}

	public function getNotSended($delay = false)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		if ($delay) {
			$r = 'SELECT * FROM `#__requests` WHERE date < ' . (time() - $delay) . ' AND sended = 0';
		} else {
			$r = 'SELECT * FROM `#__requests` WHERE sended = 0';
		}

		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssocList();
		return $data;
	}

	public function getRequests($from = 0, $to = 0)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		$r = 'SELECT * FROM `#__requests` WHERE date BETWEEN ' . $from . ' AND ' . $to;
		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssocList();
		return $data;
	}

	public function getNotSendedByTime($from, $to)
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query = 'SELECT * FROM `#__requests` WHERE date BETWEEN ' . $from . ' AND ' . $to;
		$data = $this->getAll($query);
		return $data;
	}

	public function getNotSendedScriptData()
	{

		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$r = 'SELECT * FROM `#__script_requests` WHERE sended = 0';
		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssocList();
		return $data;
	}

	public function getNotLoged()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$r = 'SELECT confirm_result, id FROM `#__requests` WHERE confirm_result != "" AND confirm_result_log = 0 AND sended = 1';
		$query->setQuery($r);
		$db->setQuery($query);
		$data = $db->loadAssocList();

		return $data;
	}

}