<?php
//sending (credit24.org.ua)
//ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define("delay", 60); // задержка заявки перед отправкой (в секундах)

// Initialize Joomla framework
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
	require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', dirname(__FILE__));
	require_once JPATH_BASE . '/includes/defines.php';
}
define('JPATH_COMPONENT', JPATH_BASE . '/components/com_sender');

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_BASE . '/includes/framework.php';

require_once JPATH_COMPONENT . '/helpers/sender.php';

$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

$db = JFactory::getDbo();

jimport('joomla.application.component.helper');

JModelLegacy::addIncludePath(JPATH_COMPONENT . '/sending', 'SendingModel');

senderSiteHelper::initLogger();

$r_model = JModelLegacy::getInstance('Request', 'SendingModel');
$mail_model = JModelLegacy::getInstance('Mail', 'SendingModel');
$ad_model = JModelLegacy::getInstance('Admitad', 'SendingModel');

function to_log($str)
{
	JLog::add($str, \JLog::DEBUG, 'com_sender');

	return false;
}


$com_sender = JComponentHelper::getComponent('com_sender');
$params = new JRegistry($com_sender->getParams());
$ad_id = $params->get('admitad_id');
$developer_mode = $params->get('developer_mode');

//todo вынести инициализацию адмитад из цикла
//todo перед отправкой помечать все заявки, чтоб избежать повторной отправки
//отправка заявок
$all_data = $r_model->getNotSended(delay);
foreach ($all_data as $key => $data) {//назначим заявкам статус "отправляется"
	$data2 = array(
		'sended' => 2,
	);

	$r_model->update($data2, $data['id']);
}

foreach ($all_data as $key => $data) {


	to_log('----------------------------');
	to_log('Обработка заявки от ' . $data['name'] . ' (' . $data['phone'] . ') (' . $data['phone2'] . ') ИНН: ' . $data['inn']);
	to_log('отправка в Адмитад-Альфа. Результат: ' . $data['alfa_result']);
	if (!$data['confirm_result_log'] and $data['confirm_result']) {
		to_log($data['confirm_result']);
		$data2 = array(
			'confirm_result_log' => 1,
		);
		$r_model->update($data2, $data['id']);
	}


	$number = substr($data['inn'], 0, 5);
	$b_date = date('d.m.Y', strtotime('01.01.1900 + ' . $number . ' days - 1 days'));
	$headers = "Content-Type: text/html; charset=utf-8\r\nFrom: tutgroshi.com.ua <form@tutgroshi.com.ua>\r\n";
	$msg = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
	<br><b>ФИО:</b> " . $data['name'] . "<br>
	<b>Телефон:</b> " . $data['phone'] . "<br>
	<b>Идентификационный код:</b> " . $data['inn'] . "<br>
	<b>Дата рождения:</b> " . $b_date . "<br>
	<b>Возраст:</b> " . $data['age'] . "<br>
	<b>Тип дохода:</b> " . $data['tip'] . "<br>
	<b>Область фактического проживания:</b> " . $data['obl'] . "<br>
	<b>Сумма кредита:</b> " . $data['sum'] . " грн.<br>
	</body></html>";

	$send = true;
	$vsi_ok = true;
	if ($data['obl'] != 'Автономная Республика Крым' and $data['obl'] != 'Луганская область' and $data['obl'] != 'Донецкая область' and $data['obl'] != 'Закарпатская область' and $data['obl'] != 'Черновицкая область' and $data['obl'] != 'Волынская область') {
		if (($data['tip2'] == "NeR" or $data['tip2'] == "RN" or $data['tip2'] == "VS" or $data['tip2'] == "m3m" or $data['tip2'] == "CPm1g")) {
			$vsi_ok = false;
			$send = true;
		} else {
			if (stristr($data['alfa_result'], "Отказано")) {
				if ($data['age'] >= 27 and $data['age'] != "до 21 года" and (!is_numeric($data['sum']) || $data['sum'] >= 20000)) {//условия передачи при ответе "отказано"
					$vsi_ok = true;
					$send = false;
				} else {
					$vsi_ok = false;
					$send = true;
				}
			} else {
				if ($data['age'] >= 27 and $data['age'] != "до 21 года" and (!is_numeric($data['sum']) || $data['sum'] >= 20000)) {//условия передачи при ответе "Успешно" и др.
					$vsi_ok = true;
					$send = false;
				} else {
					$vsi_ok = false;
					$send = true;
				}
			}
		}
	} else {
		$vsi_ok = false;
		$send = true;
	}

	if ($send) {
		$mail_model->sending_oksi($data, $headers, $msg);
		$mail_model->sending_mega($data, $headers, $msg);
		$mail_model->sending_aval($data, $headers, $msg);
		$mail_model->sending_agricol($data, $headers, $msg);
		$mail_model->sending_kredo($data, $headers, $msg);
		$mail_model->sending_cash2go($data, $headers, $msg);
		$mail_model->sending_idea($data);
		$mail_model->sending_finline($data);
		$ad_model->send($data, $ad_id, $developer_mode);
	} else {
		to_log('Заявка не прошла условие отправки в остальные банки');
	}

	//основное письмо
	$headers = "Content-Type: text/html; charset=utf-8\r\nFrom: tutgroshi.com.ua <form@tutgroshi.com.ua>\r\n";
	$phone = substr_replace("+38(" . $data['phone'], ")", 7, 0);
	$phone2 = $data['phone2'] ? substr_replace("+38(" . $data['phone2'], ")", 7, 0) : '';
	$msg = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>
	<br><b>ФИО:</b> " . $data['name'] . "<br>
	<b>Телефон:</b> " . $phone . "<br>
	<b>Телефон2:</b> " . $phone2 . "<br>
	<b>Идентификационный код:</b> " . $data['inn'] . "<br>
	<b>Возраст:</b> " . $data['age'] . "<br>
	<b>Область фактического проживания:</b> " . $data['obl'] . "<br>
	<b>Тип дохода:</b> " . $data['tip'] . "<br>
	<b>Сумма кредита:</b> " . $data['sum'] . " грн.<br>
	<b>Результат заявки Адмитад-Альфа:</b> " . $data['alfa_result'] . "<br>
	<b>Источник:</b> " . $data['referer'] . "<br>
	<b>Источник заявки:</b> " . $data['istok'] . "<br>
	<b>Девайс:</b> " . $data['device'] . "<br>
	</body></html>";

	if (mail('tg.zayavka@yandex.ru', 'заявка "' . $data['credit_type'] . '" Тутгрошi', $msg, $headers) && mail('tg.zayavka@gmail.com', 'заявка "' . $data['credit_type'] . '" Тутгрошi', $msg, $headers)) {
		to_log('отправлено на почту');
	} else {
		to_log('ошибка отправки на почту');
	}

//vsi.ok

	if ($vsi_ok) {
		if (mail('vsi.ok.zayavka@gmail.com', 'заявка "' . $data['credit_type'] . '" Тутгрошi', $msg, $headers)) {
			to_log('отправлено на почту vsi.ok.');
		} else {
			to_log('ошибка отправки на почту vsi.ok.');
		}
	} elseif ($_POST['obl'] == 'Киевская область' and $data['sum']) {
		if (mail('vsi.ok.zayavka@gmail.com', 'заявка "' . $data['credit_type'] . '" Тутгрошi', $msg, $headers)) {
			to_log('отправлено на почту vsi.ok для Кевского региона.');
		} else {
			to_log('ошибка отправки на почту vsi.ok для Кевского региона.');
		}
	}

	if ($data['log']) {
		to_log($data['log']);
	}
	to_log('конец обработки заявки от ' . $data['name']);

	$data2 = array(
		'sended' => 1,
	);

	$r_model->update($data2, $data['id']);

}

//записать в лог результаты смс-подтверждения, если нужно
$all_data = $r_model->getNotLoged();
foreach ($all_data as $key => $data) {
	to_log($data['confirm_result']);
	$data2 = array(
		'confirm_result_log' => 1,
	);
	$r_model->update($data2, $data['id']);
}

