<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 17.04.2018
 * Time: 11:00
 */
include 'api_admitad_class.php';


class Admitad extends Api_Admitad
{
	const campaign_ids = array(
		17398 => 'Кредит Днепр',
		14605 => 'Швидко Гроші',
		17288 => 'Cashpoint',
		18990 => 'Monobank',
		16243 => 'ПУМБ',
		16126 => 'PrivatBank',
		19417 => 'Unex',
		19059 => 'Ecredit',
	);

	function __construct($site_id, $path = '')
	{
		$this->site_id = $site_id;
		$this->path = $path;
		$this->getToken();
		$this->checkToken();

	}

	public function sendCreditDnepr($info, $data)
	{

		//Кредит Днепр
		if ($info['tip2'] != "VS" and
			$info['obl'] != 'Автономная Республика Крым' and $info['obl'] != 'Луганская область' and $info['obl'] != 'Донецкая область' and
			$info['obl'] != 'Черновицкая область' and $info['obl'] != 'Ивано-Франковская область' and $info['obl'] != 'Николаевская область' and
			$info['obl'] != 'Черниговская область' and $info['obl'] != 'Волынская область' and $info['obl'] != 'Закарпатская область' and
			$info['obl'] != 'Тернопольская область'
		) {
			if($info['age'] > 21  and $info['age'] != "до 21 года" and $info['age'] != "больше 70" and $info['age'] <= 65) {
				$this->to_log('Admitad. Отправка в Кредит Днепр(' . $data['last_name'] . '):');
				$respond = $this->sendRequest($data);
				$this->logResultWriter($respond);
				$this->checkLimit($data['campaigns'][0], $respond, $info['day_limit']);
			} else {
				$this->to_log('Admitad. Отправка в Кредит Днепр(' . $data['last_name'] . '): Не удовлетворяет по возрасту');
			}
		} else {
			$this->to_log('Admitad. Отправка в Кредит Днепр(' . $data['last_name'] . '): Не удовлетворяет по труду или области');
		}


	}

	public function sendGroshi($info, $data)
	{
		//Швидко Гроші
		$this->to_log('Admitad. Отправка в Швидко Гроші:');
		$respond = $this->sendRequest($data);
		$this->logResultWriter($respond);
		// $this->checkLimit($data['campaigns'][0], $respond);
	}

	public function sendCashpoint($info, $data)
	{
		//Cashpoint
		$this->to_log('Admitad. Отправка в Cashpoint:');
		$respond = $this->sendRequest($data);
		$this->logResultWriter($respond);
		// $this->checkLimit($data['campaigns'][0], $respond);
	}

	public function sendMonobank($info, $data)
	{
		//Monobank
		$this->to_log('Admitad. Отправка в Monobank:');
		$respond = $this->sendRequest($data);
		$this->logResultWriter($respond);
		// $this->checkLimit($data['campaigns'][0], $respond);
	}

	public function sendPumb($info, $data)
	{
		//Pumb
		if ($info['age'] != "до 21 года" and $info['age'] >= 25) {
			$this->to_log('Admitad. Отправка в ПУМБ:');
			$data['product_type'] = 'Кредитна картка ВСЕМОЖУ';
			$respond = $this->sendRequest($data);
			$this->logResultWriter($respond);
			$this->checkLimit($data['campaigns'][0], $respond, $info['day_limit']);
		}
	}

	public function sendPrivatBank($info, $data)
	{
		//PrivatBank
		$this->to_log('Admitad. Отправка в PrivatBank:');
		$data['product_type'] = 7;
		$respond = $this->sendRequest($data);
		$this->logResultWriter($respond);
		// $this->checkLimit($data['campaigns'][0], $respond);
	}

	public function sendUnex($info, $data)
	{
		//Unex
		$this->to_log('Admitad. Отправка в Unex:');
		$respond = $this->sendRequest($data);
		$this->logResultWriter($respond);
		//$this->checkLimit($data['campaigns'][0], $respond);
	}

	public function sendEcredit($info, $data)
	{
		//Ecredit
		$this->to_log('Admitad. Отправка в Ecredit:');
		$respond = $this->sendRequest($data);
		$this->logResultWriter($respond);
		//$this->checkLimit($data['campaigns'][0], $respond);
	}


	public function checkError($result)
	{
		if (isset($result['error_code'])) {
			switch ($result['error_code']) {
				case 0:
					if ($this->refreshToken()) {
						//запись в лог
						$this->to_log('Обновлен токен для admitad');
					}

					break;
				case 1:
					$this->to_log('Неправильный или недействительный ключ');
					break;
			}
			return true;
		}
		return false;

	}


	public function checkToken()
	{
		$resp = $this->checkRules(17398);
		if ($this->checkError($resp)) {

		}
	}


	public function checkRules($id)
	{
		$authorization = "Authorization: Bearer " . $this->token;

		$process = curl_init('https://api.admitad.com/broker/campaign_settings/' . $id . '/');
		curl_setopt($process, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded;charset=UTF-8', $authorization));
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
		$return = curl_exec($process);
		curl_close($process);
		$return = json_decode($return, 1);
		return $return;
	}


	public function checkLimit($id, $respond, $limit = false)
	{
		if (isset($respond['errors']) && !$limit) {
			$text = '';
			foreach ($respond['errors'] as $error) {
				$text = $text . (isset($error['campaign_id']) ? self::campaign_ids[$error['campaign_id']] . ': ' : '');
				if (is_array($error['message'])) {
					foreach ($error['message'] as $message) {
						$text = $text . $message[0] . ': ' . $message[1][0] . ' ';
					}
				}
			}

			if (stristr($text, 'Вы превысили ограничение по количеству заявок')) {
				global $dbh;
				$query = $dbh->prepare(
					'UPDATE campaigns SET day_limit = 1 WHERE campaign_id = ' . $id
				);
				$query->execute();
			}
		} elseif ($limit && !isset($respond['errors'])) {
			global $dbh;
			$query = $dbh->prepare(
				'UPDATE campaigns SET day_limit = 0 WHERE campaign_id = ' . $id
			);
			$query->execute();
		}

		return true;
	}

	public function incrementAd($id, $count)
	{

		global $dbh;
		$query = $dbh->prepare(
			'UPDATE campaigns SET ad_count = ' . ($count + 1) . ' WHERE campaign_id = ' . $id
		);
		$query->execute();

		return true;
	}


	public function sendRequest($data)
	{
		$data['campaigns'] = json_encode($data['campaigns']);
		$resp = $this->sendPostRequest('https://api.admitad.com/website/' . $this->site_id . '/broker/applications/create/', $data);
		return $resp;
	}

	public function getOrderList($params = array())
	{
		$urlApi = 'https://api.admitad.com/website/' . $this->site_id . '/broker/applications/';
		$request = $urlApi . "?" . http_build_query($params);
		$resp = $this->sendGetRequest($request);
		return $resp;
	}


}