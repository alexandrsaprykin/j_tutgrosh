<?php
//http://snipp.ru/view/155
class DB
{
    protected  $dsn;
    protected  $user;
    protected  $pass;

    /**
     * Объект PDO.
     */
    protected $dbh = null;

    /**
     * Statement Handle.
     */
    protected $sth = null;

    /**
     * Выполняемый SQL запрос.
     */
    public static $query = '';

    function __construct($db_name, $db_host, $user, $pass)
    {
        $this->dsn = 'mysql:dbname=' . $db_name . ';host=' . $db_host;
        $this->user = $user;
        $this->pass = $pass;
        return $this->getDbh();
    }

    /**
     * Подключение к БД
     */
    public function getDbh()
    {

        if (!$this->dbh) {
            try {
                $this->dbh = new PDO(
                    $this->dsn,
                    $this->user,
                    $this->pass,
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")
                );
                $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            } catch (PDOException $e) {
                exit('Error connecting to database: ' . $e->getMessage());
            }
        }

        return $this->dbh;
    }

    /**
     * Добавление в таблицу, в случаи успеха вернет вставленный ID, иначе 0.
     */
    public  function add($query, $param = array())
    {
        $this->sth = $this->getDbh()->prepare($query);
        return ($this->sth->execute((array)$param)) ? $this->getDbh()->lastInsertId() : 0;
    }

    /**
     * Выполнение запроса.
     */
    public  function set($query, $param = array())
    {
        $this->sth = $this->getDbh()->prepare($query);
        return $this->sth->execute((array)$param);
    }

    /**
     * Получение строки из таблицы.
     */
    public  function getRow($query, $param = array())
    {
        $this->sth = $this->getDbh()->prepare($query);
        $this->sth->execute((array)$param);
        return $this->sth->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Получение всех строк из таблицы.
     */
    public  function getAll($query, $param = array())
    {
        $this->sth = $this->getDbh()->prepare($query);
        $this->sth->execute((array)$param);
        return $this->sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Получение значения.
     */
    public function getValue($query, $param = array(), $default = null)
    {
        $result = $this->getRow($query, $param);
        if (!empty($result)) {
            $result = array_shift($result);
        }

        return (empty($result)) ? $default : $result;
    }

    /**
     * Получение столбца таблицы.
     */
    public function getColumn($query, $param = array())
    {
        $this->sth = $this->getDbh()->prepare($query);
        $this->sth->execute((array)$param);
        return $this->sth->fetchAll(PDO::FETCH_COLUMN);
    }
}