<header class="header">
    <div class="container">
        <!-- 768-992 Показан "Начало"-->
        <div class="row hidden-xs hidden-md hidden-lg">
            <div class="col-sm-5">
                <div class="header_logo">
                    <a href="/">
                        <picture>
                            <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-credit.svg">
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-credit.png" alt="Logo">
                        </picture>
                        <h2 class="hidden-xs">тутгрошi</h2>
                    </a>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="header_desk_top">
                    <picture>
                        <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-small.svg">
                        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-small.png" class="hidden-xs hidden-sm" alt="Logo">
                    </picture>
                    <p class="header_desk_title">Наличный кредит</p>
                    <a href="<?php echo JRoute::_('/?page=cart') ?>" class="header_desk_btn">Кредитная карта</a>
                </div>
            </div>
        </div>
        <div class="row hidden-xs hidden-md hidden-lg">
            <div class="col-sm-12">
                <div class="header_desk_bottom">
                    <ul class="header_number">
                        <li>(067)<a href="#">537-26-58</a></li>
                        <li>(050)<a href="#">837-23-25</a></li>
                        <li>(044)<a href="#">585-59-66</a></li>
                        <li class="header_recall"><a href="#recall" data-toggle="modal">Перезвоните мне</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- 768-992 Показан "Конец"-->
        <!-- Показан на 320-768 "НАЧАЛО"" -->
        <div class="row hidden-sm hidden-md hidden-lg">
            <div class="col-xs-12">
                <div class="col-xs-4">
                    <div class="header_logo">
                        <a href="/">
                            <picture>
                                <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-credit.svg">
                                <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-credit.png" alt="Logo">
                            </picture>
                            <h2 class="hidden-xs">тутгрошi</h2>
                        </a>
                    </div>
                </div>
                <div class="col-xs-8">
                    <div class="header_desk_top">
                        <picture>
                            <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-small.svg">
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-small.png" class="hidden-xs" alt="Logo">
                        </picture>
                        <p class="header_desk_title">Наличный кредит</p>
                        <a href="<?php echo JRoute::_('/?page=cart') ?>" class="header_desk_btn">Кредитная карта</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
            <div class="header_desk_bottom">
                <ul class="header_number">
                    <li>(067)<a href="#">537-26-58</a></li>
                    <li>(050)<a href="#">837-23-25</a></li>
                    <li>(044)<a href="#">585-59-66</a></li>
                    <li class="header_recall"><a href="#recall" data-toggle="modal">Перезвоните мне</a></li>
                </ul>
            </div>
        </div>
        <!-- Показан на 320-768 "Конец"" -->
        <!-- 320-992 Спрятано "Начало" -->
        <div class="row hidden-xs hidden-sm">
            <div class="col-md-3">
                <div class="header_logo">
                    <a href="/">
                        <picture>
                            <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-credit.svg">
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-credit.png" alt="Logo">
                        </picture>
                        <h2 class="hidden-xs">тутгрошi</h2>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="header_desk">
                    <div class="col-md-12">
                        <div class="header_desk_top">
                            <picture>
                                <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-small.svg">
                                <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-small.png" class="hidden-xs hidden-md" alt="Logo">
                            </picture>
                            <p class="header_desk_title">Наличный кредит</p>
                            <a href="<?php echo JRoute::_('/?page=cart') ?>" class="header_desk_btn">Кредитная карта</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="header_desk_bottom">
                            <picture>
                                <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/phone.svg">
                                <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/phone.png" class="hidden-xs hidden-md" alt="Phone">
                            </picture>
                            <ul class="header_number">
                                <li>(067)<a href="#">537-26-58</a></li>
                                <li>(050)<a href="#">837-23-25</a></li>
                                <li>(044)<a href="#">585-59-66</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="header_recall"><a href="#recall" data-toggle="modal">Перезвоните мне</a></div>
            </div>
        </div>
        <!-- 320-992 Спрятано "Конец" -->
    </div>
</header>
