<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-6">
                <div class="col-md-6 col-xs-12">
                    <div class="footer_logo">
                        <picture>
                            <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-footer.svg">
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-footer.png" alt="Logo">
                        </picture>
                        <h2 class="hidden-xs">тутгрошi</h2>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <p class="footer_title">Максимальный шанс получения
                        кредита с минимальным процентом</p>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="footer_desk_bottom">
                        <picture>
                            <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/footer_phone.svg">
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/footer_phone.png" alt="Phone">
                        </picture>
                        <ul class="footer_number">
                            <li>(067)<a href="#">537-26-58</a></li>
                            <li>(050)<a href="#">837-23-25</a></li>
                            <li>(044)<a href="#">585-59-66</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="footer_desk_address">
                        <picture>
                            <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/icon-map.svg">
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/icon_map.png" alt="Phone">
                        </picture>
                        <p>Киев, ул. Богдана Хмельницкого, 17/52</p>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <p class="footer_ask_title">Остались вопросы? Пишите!</p>
                    <a href="#question" data-toggle="modal" class="footer_ask">Задать вопрос</a>
                </div>
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <p class="footer_license_title">Наши договоры и лицензии</p>
                    <a href="#documents" data-toggle='modal' class="footer_license">Ознакомиться</a>
                </div>
                <div class="col-md-12 col-xs-12">
                    <div class="footer_copy">
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <p>&copy; ТутГрошi, 2013-2017. Киев</p>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <p>Дизайн – <span class="yellow">Александр Льдов</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 hidden-xs hidden-sm hidden-md">
                <div class="yandex">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2540.651109237619!2d30.50431131551387!3d50.44759897947494!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4ce58ae31ed7b%3A0xd3a0d776c9f876f8!2z0LLRg9C70LjRhtGPINCR0L7Qs9C00LDQvdCwINCl0LzQtdC70YzQvdC40YbRjNC60L7Qs9C-LCA1Miwg0JrQuNGX0LIsINCj0LrRgNCw0LjQvdCw!5e0!3m2!1sru!2sby!4v1497880264671" width="100%" height="504" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</footer>