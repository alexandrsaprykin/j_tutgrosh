<div class="groshi_main">
    <div class="container">
        <div class="groshi_main_center">
            <picture>
                <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-shape.svg">
                <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-shape.png" alt="Logo">
            </picture>
            <h1 class="groshi_main_title">тутгрошi</h1>
            <p class="groshi_main_desk">Максимальный шанс получения кредита с минимальным процентом</p>

			<?php include 'modal/sms_confirm.php'; ?>
        </div>
    </div>
</div>
