<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 20.01.2019
 * Time: 12:54
 */
?>
<!-- Header -->
<?php include 'credit/header_credit.php'; ?>
<!-- Credit Info -->
<section class="credit_info">
    <div class="col-md-7 col-sm-6">
        <div class="credit_info_desk cart_bg">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="credit_info_desk_title">
                                <h2>Кредиты  с <span>ТутГрошi</span></h2>
                                <p><span>Максимальный</span> шанс получения кредита<br/>
                                    с <span class="line">минимальным</span> процентом</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="credit_info_advantages card_after">
            <div class="credit_info_advantages_title">
                <h3>Кредитный калькулятор</h3>
            </div>
            <div class="credit_info_advantages_list range">
                <div class="range_item">
                    <div class="range_item_title">
                        <p>Сумма кредита</p>
                        <p><input type="text" data-changed = 0 id="amount" readonly> гривен</p>
                    </div>
                    <div id="slider-range-min" min="0"></div>
                </div>
                <div class="range_item">
                    <div class="range_item_title">
                        <p>Срок погашения</p>
                        <p><input type="text" id="amount2" readonly> месяцев</p>
                    </div>
                    <div id="slider-range-min2" min="0"></div>
                </div>
            </div>
            <div class="credit_info_advantages_rules credit_card_info">
                <p>Оставьте заявку <span class="sale"> <?php echo ((date('j', time()/* + 86400*/)).' '. $monthes[(date('n', time() /*+ 86400*/))]); ?></span> и получите дополнительную <span class="sale">скидку -5%</span> по ставке!
                    Ваш эжемесячный платеж будет:</p>
                <h3 class="yellow credit_yellow">
                    <span id="calc_rez" class="permonth">406</span> <span class="coin">грн</span>
                </h3>
                <p class="credit_after">При заявке <span class="sale">после <?php echo ((date('j')).' '. $monthes[(date('n'))]); ?>:</span></p>
                <p class="credit_price">
                    <span class="credit_after_price" id="calc_rez2">455</span>
                    <span class="coin">грн</span></p>
                <div class="credit_info_advantages_btn">
                    <a href="#credit" id="calc_button" data-toggle="modal">Оформить кредит бесплатно!</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Why -->
<section class="why">
    <div class="container">
        <div class="why_us">
            <div class="row">
                <div class="col-md-12">
                    <div class="why_us_title">Почему выгодно брать <span>кредит с нами</span></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="why_us_item card_item">
                        <picture>
                            <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/hand.svg">
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/hand.png" alt="Hand">
                        </picture>
                        <h4>Процентная ставка <span class="gray line-del">45%</span> <span class="blue">12,99%*</span></h4>
                        <p>*совместная программа ТутГроші и банков-партнеров предусматривает понижение ставки <span class="blue">до 12,99%</span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="why_us_item">
                        <picture>
                            <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/time.svg">
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/time.png" alt="Time">
                        </picture>
                        <h4>Экономия времени</h4>
                        <p>Одна заявка во все банки, решение по кредиту <br/>
                            в течение <span class="blue">10 минут</span></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="why_us_item">
                        <picture>
                            <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/person.svg">
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/person.png" class="person" alt="Person">
                        </picture>
                        <h4>Никаких походов и очередей</h4>
                        <p>Решение по кредиту Вы получаете <span class="blue">без необходимости</span> <br/>посещения банка</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="why_us_item">
                        <picture>
                            <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/like.svg">
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/like.png" alt="Like">
                        </picture>
                        <h4>Максимальный шанс</h4>
                        <p>Вероятность положительного решения по кредиту –<br/> <span class="blue">99 процентов</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banks -->
<section class="banks">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banks_title">
                    Банки соревнуются <span>за Вас</span>
                </div>
            </div>
        </div>
        <div class="banks_items">
            <div class="col-md-1 col-sm-2 hidden-xs">
                <div class="banks_left">
                    <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/left.svg" class="back" alt="Left Arrow">
                </div>
            </div>
            <div class="col-md-10 col-sm-8">
                <div class="banks_slider carousel">
                    <div class="banks_slider_item">
                        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/monobank.jpg" alt="Monobank">
                    </div>
                    <div class="banks_slider_item">
                        <a href="https://www.ideabank.ua/uk/about/partners/kredytni-poserednyky/" id="idea-partner"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/ideabank.png" alt="IdeaBank"></a>
                    </div>
                    <div class="banks_slider_item">
                        <a href="https://alfabank.ua/private-persons/partneri/" id="alfa-partner"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/alfa.png" alt="Альфа Банк"></a>
                    </div>
                    <div class="banks_slider_item">
                        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/opt.png" alt="OtpBank">
                    </div>
                    <div class="banks_slider_item">
                        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/dnipro.png" alt="Кредит Днипро">
                    </div>
                    <div class="banks_slider_item">
                        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/universal.png" alt="Universal Bank">
                    </div>
                    <div class="banks_slider_item">
                        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/tac.png" alt="ТАСКОМБАНК">
                    </div>

                    <div class="banks_slider_item">
                        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/monobank.jpg" alt="Monobank">
                    </div>
                    <div class="banks_slider_item">
                        <a href="https://www.ideabank.ua/uk/about/partners/kredytni-poserednyky/" id="idea-partner"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/ideabank.png" alt="IdeaBank"></a>
                    </div>
                    <div class="banks_slider_item">
                        <a href="https://alfabank.ua/private-persons/partneri/" id="alfa-partner"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/alfa.png" alt="Альфа Банк"></a>
                    </div>
                    <div class="banks_slider_item">
                        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/opt.png" alt="OtpBank">
                    </div>
                    <div class="banks_slider_item">
                        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/dnipro.png" alt="Кредит Днипро">
                    </div>
                    <div class="banks_slider_item">
                        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/universal.png" alt="Universal Bank">
                    </div>
                    <div class="banks_slider_item">
                        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/tac.png" alt="ТАСКОМБАНК">
                    </div>
                </div>
            </div>
            <div class="col-md-1 col-xs-6 hidden-sm hidden-md hidden-lg">
                <div class="banks_left">
                    <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/left.svg" class="back-xs" alt="Left Arrow">
                </div>
            </div>
            <div class="col-md-1 col-xs-6 col-sm-2 hidden-md hidden-lg">
                <div class="banks_right">
                    <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/right-xs.svg" class="next-xs" alt="Right Arrow">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 hidden-md hidden-lg">
                <p class="tooltip">Листайте <span class="blue">вправо</span> или <span class="blue">влево</span>,
                    чтобы увидеть больше банков</p>
            </div>
            <div class="col-md-1 hidden-xs hidden-sm">
                <div class="banks_right">
                    <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/right.svg" class="next" alt="Right Arrow">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Option Rules -->
<section class="option_rules">
    <div class="col-md-7">
        <div class="option_rules_desk">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="option_rules_desk_title">Условия   <span>кредитования</span>
                        </div>
                        <div class="option_rules_desk_list">
                            <ul>
                                <li>Сумма от <span>1 000</span> до <span>200 000</span> гривен</li>
                                <li>Ваш возраст от <span>21</span> до <span>75</span> лет </li>
                                <li>Ставка от <span>12,99%</span> годовых</li>
                                <li><span>Все регионы</span> Украины, кроме АРК</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="option_rules_scheme">
            <h2>Схема <span>работы</span></h2>
            <a href="#credit" data-toggle="modal" class="option_rules_scheme_btn">Оформляете заявку на кредит</a>
            <div class="step"></div>
            <p>Заявка отправляется
                во все банки Украины</p>
            <div class="step"></div>
            <p>Мы связываемся с Вами
                и предалагаем самый
                выгодный вариант</p>
        </div>
    </div>
</section>
<!-- Feedback -->
<section class="feedback">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="feedback_title">Отзывы клиентов <span>ТутГрошi</span></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1 hidden-xs col-sm-2">
                <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/left.svg" class="prev" alt="Arrow Left">
            </div>
            <div class="col-md-10 col-sm-8">
                <div class="feedback_slider slick">
                    <div class="feedback_slider_item">
                        <div class="top">
                            <div class="name hidden-sm hidden-md hidden-lg">
                                <h4>Юлия</h4>
                                <p>продавец, г. Херсон</p>
                            </div>
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/ТГ1_min.jpg" alt="Feedback">
                            <div class="name hidden-xs">
                                <h4>Юлия</h4>
                                <p>продавец, г. Херсон</p>
                            </div>
                        </div>
                        <div class="bottom">
                            <p>Искала где бы оформить крединую карту и как и многие другие оформила сдесь. Очень удобно, рекомендую! </p>
                        </div>
                    </div>

                    <div class="feedback_slider_item">
                        <div class="top">
                            <div class="name hidden-sm hidden-md hidden-lg">
                                <h4>Сергій</h4>
                                <p>43 роки, м. Луцьк</p>
                            </div>
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/ТГ2_min.jpg" alt="Feedback">
                            <div class="name hidden-xs">
                                <h4>Сергій</h4>
                                <p>43 роки, м. Луцьк</p>
                            </div>
                        </div>
                        <div class="bottom">
                            <p>Класно що можна оформити кредит через інтернет. Вибрав банк, заповнив дані свої в анкеті і відразу отримав рішення. Зателефонували з банку і сказали в який день можна прийти за грошима. Дякую! Це супер! Рекомендую!</p>
                        </div>
                    </div>
                    <div class="feedback_slider_item">
                        <div class="top">
                            <div class="name hidden-sm hidden-md hidden-lg">
                                <h4>Александр</h4>
                                <p>предприниматель. г. Киев </p>
                            </div>
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/ТГ7_min.jpg" alt="Feedback">
                            <div class="name hidden-xs">
                                <h4>Александр</h4>
                                <p>предприниматель. г. Киев </p>
                            </div>
                        </div>
                        <div class="bottom">
                            <p>Покупал кухню, сосед посоветовал взять кредит через интернет. Получилось все очень удобно, заполняеш анкету и почти сразу получаеш ответ по телефону. Сказали куда прийти за деньгами. Рекоментую.</p>
                        </div>
                    </div>
                    <div class="feedback_slider_item">
                        <div class="top">
                            <div class="name hidden-sm hidden-md hidden-lg">
                                <h4>Михайло</h4>
                                <p>начальник автоколони, м. Хмельницький</p>
                            </div>
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/ТГ3_min.jpg" alt="Feedback">
                            <div class="name hidden-xs">
                                <h4>Михайло</h4>
                                <p>начальник автоколони, м. Хмельницький</p>
                            </div>
                        </div>
                        <div class="bottom">
                            <p>Хотіли  купити автомобіль, але половини коштів не вистачало (чекати поки назбираємо не хотілось &#9786;)Ходили по банках шукали чи є кредит на автомобілі, але, нажаль, такі умови ставили, що в нас бажання зникало, а потім знайомі порадили взяти кредит через інтернет – і все вийшло! Залишили заявку на сайті (тільки не вказували, що гроші на авто, а просто грошовий кредит) і рішення відразу було погоджене. З банку привітний працівник зателефонував і назначив дату отримання кредиту.</p>
                        </div>
                    </div>
                    <div class="feedback_slider_item">
                        <div class="top">
                            <div class="name hidden-sm hidden-md hidden-lg">
                                <h4>Виктор</h4>
                                <p>45 лет. Киев. </p>
                            </div>
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/no_photo_man.jpg" alt="Feedback">
                            <div class="name hidden-xs">
                                <h4>Виктор</h4>
                                <p>45 лет. Киев. </p>
                            </div>
                        </div>
                        <div class="bottom">
                            <p>Деньги получил, спасибо. без лишней волокиты.</p>
                        </div>
                    </div>
                    <div class="feedback_slider_item">
                        <div class="top">
                            <div class="name hidden-sm hidden-md hidden-lg">
                                <h4>Надежда Анатольевна</h4>
                                <p>пенсионер, г. Киев</p>
                            </div>
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/ТГ4_min.jpg" alt="Feedback">
                            <div class="name hidden-xs">
                                <h4>Надежда Анатольевна</h4>
                                <p>пенсионер, г. Киев</p>
                            </div>
                        </div>
                        <div class="bottom">
                            <p>Оформляла кредит через сдесь на сайте, но заполнила анкету в четверг, а решение получила только в пятницу, хотя писало что решение будет через 5 минут.</p>
                        </div>
                    </div>
                    <div class="feedback_slider_item">
                        <div class="top">
                            <div class="name hidden-sm hidden-md hidden-lg">
                                <h4>Оксана</h4>
                                <p>бухгалтер, г. Бердянск, Запорожская область</p>
                            </div>
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/ТГ5_min.jpg" alt="Feedback">
                            <div class="name hidden-xs">
                                <h4>Оксана</h4>
                                <p>бухгалтер, г. Бердянск, Запорожская область</p>
                            </div>
                        </div>
                        <div class="bottom">
                            <p>Брали кредит на ремонт квартиры. Воспользовались акцией меньше % ставка по кредиту. Звонило нескольоко банков, но сумму которую мы хотели мог выдать только один Универсал, пришлось ехать в Запорожье, так как в нашем городе не было отделения. Но все ровно очень довольны в банке была специальная программа по кредитам для ремонта.</p>
                        </div>
                    </div>
                    <div class="feedback_slider_item">
                        <div class="top">
                            <div class="name hidden-sm hidden-md hidden-lg">
                                <h4>Олександр</h4>
                                <p>військововслужбовець, Чернівці</p>
                            </div>
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/no_photo_man.jpg" alt="Feedback">
                            <div class="name hidden-xs">
                                <h4>Олександр</h4>
                                <p>військововслужбовець, Чернівці</p>
                            </div>
                        </div>
                        <div class="bottom">
                            <p>Піднялось питання збільшення житлової площі, вирішили взяти з дружиною кредит та інвестувати в будівництво. Коли ходили разом по банках, нам пропонували оформити кредит тільки на когось одного, а інший мав  виступати як поручитель. А потім  лишили заявки через інтернет в різних банках, і в нас вийшло – оформили кредит на дружину і на мене. Зарплата дозволяє нам обом платити по кредиту. Правда на дружину трохи меншу суму оформили. &#9785;
                                Але сума отриманого кредиту в загальному вийшла більша, ніж би я взяв тільки на себе кредит
                            </p>
                        </div>
                    </div>
                    <div class="feedback_slider_item">
                        <div class="top">
                            <div class="name hidden-sm hidden-md hidden-lg">
                                <h4>Павел</h4>
                                <p>частный предприниматель, Кузнецовск, Ровенская область</p>
                            </div>
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/ТГ6_min.jpg" alt="Feedback">
                            <div class="name hidden-xs">
                                <h4>Павел</h4>
                                <p>частный предприниматель, Кузнецовск, Ровенская область</p>
                            </div>
                        </div>
                        <div class="bottom">
                            <p>Получил кредит 135 тыс. грн. на СПД, потребовали отчетность за прошлый год. Кредит на 5 лет получился. </p>
                        </div>
                    </div>
                    <div class="feedback_slider_item">
                        <div class="top">
                            <div class="name hidden-sm hidden-md hidden-lg">
                                <h4>Любовь Васильевна</h4>
                                <p>66 лет. Днепропетровск</p>
                            </div>
                            <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/ТГ8_min.jpg" alt="Feedback">
                            <div class="name hidden-xs">
                                <h4>Любовь Васильевна</h4>
                                <p>66 лет. Днепропетровск</p>
                            </div>
                        </div>
                        <div class="bottom">
                            <p>Мне выдали хотя была просточка в Привате, с пенсией 2600 грн, кредит получился 46600 грн., ставку не помню.</p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-1 col-xs-6 hidden-sm hidden-md hidden-lg">
                <div class="banks_left">
                    <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/left.svg" class="prev" alt="Left Arrow">
                </div>
            </div>
            <div class="col-md-1 col-xs-6 hidden-sm hidden-md hidden-lg">
                <div class="banks_right">
                    <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/right-xs.svg" class="forward" alt="Right Arrow">
                </div>
            </div>
            <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
                <p class="tooltip">Листайте <span class="blue">вправо</span> или <span class="blue">влево</span>,
                    чтобы увидеть больше отзывов</p>
            </div>
            <div class="col-md-1 hidden-xs col-sm-2">
                <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/right-xs.svg" class="forward" alt="Arrow Right">
            </div>
            <div class="col-xs-12 hidden-xs hidden-md hidden-lg">
                <p class="tooltip">Листайте <span class="blue">вправо</span> или <span class="blue">влево</span>,
                    чтобы увидеть больше отзывов</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <a href="#feedback" data-toggle="modal" class="feedback_btn">Оставить отзыв</a>
            </div>
        </div>
    </div>
</section>
<!-- Application -->
<section class="application">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12 col-lg-8">
                    <div class="application_title">
                        <h3>Уже одобрено <span>22 391</span> заявок. Ваша может стать следующей!</h3>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <a class="application_btn" href="#credit" data-toggle="modal">Оформить кредит бесплатно</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Footer -->
<?php include 'credit/footer_credit.php'; ?>


<!-- Заявка на кредит -->
<div class="modal" tabindex="-" role="dialog" id="credit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Заявка на кредит</h4>
            </div>
            <div class="modal-body">
                <p class="modal-desk">Заполните и сразу получите ответ!</p>
				<?php
				$cart = false;
				include 'modal/form.php'; ?>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter23319964 = new Ya.Metrika({
                    id:23319964,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://d31j93rd8oukbv.cloudfront.net/metrika/watch_ua.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/23319964" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Google Analytics counter --><!-- /Google Analytics counter -->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

<script>

    (adsbygoogle = window.adsbygoogle || []).push({

        google_ad_client: "ca-pub-2934349661479573",

        enable_page_level_ads: true

    });

</script>
<script type="text/javascript">var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-46305591-1']);
    _gaq.push(['_trackPageview']);
    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>