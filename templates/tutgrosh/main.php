<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 20.01.2019
 * Time: 12:54
 */
?>
<div class="groshi_main">
    <div class="container">
        <div class="groshi_main_center">
            <picture>
                <source srcset="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-shape.svg">
                <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo-shape.png" alt="Logo">
            </picture>
            <h1 class="groshi_main_title">тутгрошi</h1>
            <p class="groshi_main_desk">Максимальный шанс получения кредита с минимальным процентом</p>
            <h2 class="groshi_main_choose">Вы хотите <span>получить:</span></h2>
            <div class="groshi_main_center_item column">
                <div class="groshi_main_item credit" onclick="location='<?php echo JRoute::_('/?page=credit') ?>'" style="cursor: pointer">
                    <div class="anchor">
                        <a href="<?php echo JRoute::_('/?page=credit') ?>">Наличный кредит</a>
                    </div>
                </div>
                <div class="groshi_main_item credit_card" onclick="location='<?php echo JRoute::_('/?page=cart') ?>'" style="cursor: pointer">
                    <div class="anchor">
                        <a href="<?php echo JRoute::_('/?page=cart') ?>">Кредитную карту</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="groshi_main_line"></div>
    <div class="groshi_main_footer"><p>&copy; ТутГрошi, 2019. Киев.</p></div>
</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter23319964 = new Ya.Metrika({
                    id:23319964,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://d31j93rd8oukbv.cloudfront.net/metrika/watch_ua.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/23319964" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
