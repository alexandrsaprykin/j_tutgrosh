<div class="modal" tabindex="-1" role="dialog" id="feedback">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Оставить отзыв</h4>
            </div>
            <div class="modal-body">
                <div class="modal-desk">После проверки менеджером Ваш отзыв
                    добавится на сайт!</div>
                <form class="second_form feedapply">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="name" id="full_name" placeholder="Ваше Имя" required>
                        </div>
                        <div class="col-md-12">
                            <input name="worker" type="text" id="worker" placeholder="Кем работаете">
                        </div>
                        <div class="col-md-12">
                            <input type="text" name="location" id="location" placeholder="Место жительства">
                        </div>
                        <div class="col-md-12">
                            <textarea name="feed" id="feed" placeholder="Напишите отзыв"></textarea>
                        </div>
                        <div class="col-md-12">
                            <input type="hidden" name="type" value="Отзыв">
                            <button type="submit" class="submit">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
