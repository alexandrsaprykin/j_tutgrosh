<form class="second_form" action="<?php echo JRoute::_('/index.php?option=com_sender&view=form&task=form.newRequest') ?>" method="POST" onSubmit="return checkedForm(this)">
    <div class="row">
        <div class="col-md-12">
            <label for="full_name" id="name_error" class="error">Укажите полностью свою фамилию, имя и отчество</label>
            <input type="text" class="for-ajax" name="contact_name" id="full_name" placeholder="Фамилия Имя Отчество"
                   required>
        </div>
        <div class="col-md-12">
            <div class="phone_input">
                <div class="phone_code">+38</div>
                <input name="phone" class="for-ajax" type="text" data-id="phone1" id="phone1" placeholder="Код"
                       required>
                <input name="phone2" class="for-ajax" type="text" data-id="phone2" id="phone2"
                       placeholder="Номер телефона" required>
            </div>
        </div>
        <div class="col-md-12">
            <div class="phone_input">
                <div class="phone_code">+38</div>
                <input name="phone1_2" class="for-ajax" type="text" data-id="phone1_2" id="phone1_2" placeholder="Код">
                <input name="phone2_2" class="for-ajax" type="text" data-id="phone2_2" id="phone2_2"
                       placeholder="Номер телефона2 (не обязательно)">
            </div>
        </div>
        <div class="col-md-12">
            <input type="text" class="for-ajax" name="idencod" id="code" placeholder="Идентификационный номер" required>
        </div>
        <div class="col-md-12">
            <select id="age" class="for-ajax" name="age">
                <option value="" selected disabled class="selected">Возраст</option>
                <option value="до 21 года">до 21 года</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
                <option value="32">32</option>
                <option value="33">33</option>
                <option value="34">34</option>
                <option value="35">35</option>
                <option value="36">36</option>
                <option value="37">37</option>
                <option value="38">38</option>
                <option value="39">39</option>
                <option value="40">40</option>
                <option value="41">41</option>
                <option value="42">42</option>
                <option value="43">43</option>
                <option value="44">44</option>
                <option value="45">45</option>
                <option value="46">46</option>
                <option value="47">47</option>
                <option value="48">48</option>
                <option value="49">49</option>
                <option value="50">50</option>
                <option value="51">51</option>
                <option value="52">52</option>
                <option value="53">53</option>
                <option value="54">54</option>
                <option value="55">55</option>
                <option value="56">56</option>
                <option value="57">57</option>
                <option value="58">58</option>
                <option value="59">59</option>
                <option value="60">60</option>
                <option value="61">61</option>
                <option value="62">62</option>
                <option value="63">63</option>
                <option value="64">64</option>
                <option value="65">65</option>
                <option value="66">66</option>
                <option value="67">67</option>
                <option value="68">68</option>
                <option value="69">69</option>
                <option value="70">70</option>
                <option value="больше 70">больше 70</option>
            </select>
        </div>
        <div class="col-md-12">
            <select name="obl" id="obl" class="for-ajax" required>
                <option value="" selected disabled class="selected">Область фактического проживания</option>
                <option value="Автономная Республика Крым">Автономная Республика Крым</option>
                <option value="Винницкая область">Винницкая область</option>
                <option value="Волынская область">Волынская область</option>
                <option value="Днепропетровская область">Днепропетровская область</option>
                <option value="Донецкая область">Донецкая область</option>
                <option value="Житомирская область">Житомирская область</option>
                <option value="Закарпатская область">Закарпатская область</option>
                <option value="Запорожская область">Запорожская область</option>
                <option value="Ивано-Франковская область">Ивано-Франковская область</option>
                <option value="Киевская область">Киевская область</option>
                <option value="Кировоградская область">Кировоградская область</option>
                <option value="Луганская область">Луганская область</option>
                <option value="Львовская область">Львовская область</option>
                <option value="Николаевская область">Николаевская область</option>
                <option value="Одесская область">Одесская область</option>
                <option value="Полтавская область">Полтавская область</option>
                <option value="Ровненская область">Ровненская область</option>
                <option value="Сумская область">Сумская область</option>
                <option value="Тернопольская область">Тернопольская область</option>
                <option value="Харьковская область">Харьковская область</option>
                <option value="Херсонская область">Херсонская область</option>
                <option value="Хмельницкая область">Хмельницкая область</option>
                <option value="Черкасская область">Черкасская область</option>
                <option value="Черниговская область">Черниговская область</option>
                <option value="Черновицкая область">Черновицкая область</option>
            </select>
        </div>
        <div class="col-md-12">
            <select name="tip" class="for-ajax" id="tip" required>
                <option value="" selected disabled class="selected">Тип дохода</option>
                <option value="Я работаю официально">Я работаю официально</option>
                <option value="Я военнослужащий">Я военнослужащий</option>
                <option value="Я получаю пенсию">Я получаю пенсию</option>
                <option value="Я частный предприниматель">Я частный предприниматель</option>
                <option value="Я работаю неофициально">Я работаю неофициально</option>
                <option value="Я не работаю">Я не работаю</option>
            </select>
            <select name="work_duration" id="tip2" class="for-ajax" style="display: none;">
                <option value="">Как давно работаете на последнем месте работы?</option>
                <option value="официально больше 3">больше 3 месяцев на последнем месте работы</option>
                <option value="официально меньше 3">меньше 3 месяцев на последнем месте работы</option>
            </select>
            <select name="reg_duration" id="tip3" class="for-ajax" style="display: none;">
                <option value="">Как давно Вы зарегистрированы как ЧП?</option>
                <option value="больше 1 года">зарегистрирован как предприниматель больше 1 года</option>
                <option value="меньше 1 года">зарегистрирован как предприниматель меньше 1 года</option>
            </select>
        </div>
        <div class="col-md-12">
            <input type="text" name="summa" id="sum" class="input_sum for-ajax" required
                   placeholder="Сумма кредита в гривнах">
            <div id="fsogl">
                <input type="checkbox" checked id="confirm" class="checkbox" name="sogl" required>
                <label class="label" for="confirm" id="confirm_text">Я даю согласие на обработку персональных данных и
                    соглашаюсь с <a href="#agree" data-toggle="modal" target="_blank" class="agree">пользовательским
                        соглашением</a></label>
            </div>
            <input type='hidden' name='device' id='device' class="for-ajax" value='Не определено'/>
            <input type='hidden' class="for-ajax" name='referer' value='<? echo $_SESSION['ref']; ?>'/>
            <input type='hidden' id="trigger" class="for-ajax" name='sended' value='false'/>
            <input type='hidden' class="for-ajax" name='type' value='<? echo ($cart) ? 'Карта' : 'Кредит' ?>'/>
            <input type='hidden' class="for-ajax" name='ok' value='order'/>
            <input type='hidden' class="for-ajax" name='istok' value='<?
			$ref = $_SERVER['REQUEST_URI'];
			$ref2 = $_SERVER['HTTP_REFERER'];
			if (strpos($ref, 'utm_campaign')) {
				$istok = substr($ref, strpos($ref, 'utm_campaign') + 13);
				if (strpos($istok, '&')) {
					$istok = substr($istok, 0, strpos($istok, '&'));
					setcookie("istok", $istok);
				}
			} elseif (strpos($ref2, 'utm_campaign')) {
				$istok = substr($ref2, strpos($ref2, 'utm_campaign') + 13);
				if (strpos($istok, '&')) {
					$istok = substr($istok, 0, strpos($istok, '&'));
					setcookie("istok", $istok);
				}
			} else {
				$istok = 'Sourcenotdefined';
				if ($_COOKIE['istok']) {
					$istok = $_COOKIE['istok'];
				}
			}
			echo $istok; ?>'/>
        </div>
    </div>
    <span style="font-size: 11px; line-height: 10px; color: red;margin-top: 15px; display: block;">Внимание! После отправки заявки введите проверочный код, указаный в SMS!</span>
    <button type="submit" name="form_button" class="submit">Отправить заявку</button>
</form>


<script>
    // проверка формы -----------------------------------
    function checkedForm(form) {
        var d = 1;
        var RegEx = /\s/g;
        var RegEx2 = /\(|\)|\s|-/g;
        var RegEx3 = /^\+380|380|038/g;

        if (form.contact_name.value == "") {
            form.contact_name.style.border = "1px solid #FF0000";
            d = 0;
        } else {
            form.contact_name.style.border = "1px solid #CCCCCC";
        }

        var name_arr = form.contact_name.value.split(' ');
        if (name_arr.length > 1 && name_arr[0].replace(RegEx, "") != '' && name_arr[1].replace(RegEx, "") != '') {
            $('label#name_error').css('display', 'none');
        }
        else {
            form.contact_name.style.border = "1px solid #FF0000";
            d = 0;
            $('label#name_error').css('display', 'block')
            form.contact_name.scrollIntoView(false);
            form.contact_name.focus();
        }

        var phone_val = form.phone.value + form.phone2.value;

        phone_val = phone_val.replace(RegEx2, "");

        var phone2_val = form.phone1_2.value + form.phone2_2.value;

        phone2_val = phone2_val.replace(RegEx2, "");

        if (form.phone2.value == "" || form.phone1.value == "" || phone_val.length != 10 || RegEx3.test(phone_val)) {
            form.phone1.focus();
            form.phone1.scrollIntoView(false);
            setTimeout(function() {alert('Некорректный номер телефона!');}, 500);

            form.phone1.style.border = "1px solid #FF0000";
            form.phone2.style.border = "1px solid #FF0000";
            d = 0;
        } else {
            form.phone1.style.border = "1px solid #CCCCCC";
            form.phone2.style.border = "1px solid #CCCCCC";
        }


        var iden_val = form.idencod.value;

        iden_val = iden_val.replace(RegEx, "");
        if (form.idencod.value == "" || iden_val.length != 10) {
            form.idencod.focus();
            form.idencod.style.border = "1px solid #FF0000";
            d = 0;
        } else {
            form.idencod.style.border = "1px solid #CCCCCC";
        }

        if (form.age.value == "") {
            form.age.focus();
            document.getElementById('age').style.border = "1px solid #FF0000";
            d = 0;
        } else {
            document.getElementById('age').style.border = "1px solid #CCCCCC";
        }

        if (form.obl.value == "") {
            form.obl.focus();
            document.getElementById('obl').style.border = "1px solid #FF0000";
            d = 0;
        } else {
            document.getElementById('obl').style.border = "1px solid #CCCCCC";
        }

        if (form.tip.value == "") {
            form.tip.focus();
            document.getElementById('tip').style.border = "1px solid #FF0000";
            d = 0;
        } else {
            document.getElementById('tip').style.border = "1px solid #CCCCCC";
        }

        if (form.summa.value == "") {
            form.summa.focus();
            form.summa.style.border = "1px solid #FF0000";
            d = 0;
        } else {
            form.summa.style.border = "1px solid #CCCCCC";
        }

        if (form.sogl.checked) {
            document.getElementById('fsogl').style.color = "#888888";
        } else {
            document.getElementById('fsogl').style.color = "#FF0000";
            d = 0;
        }

        if (d == 1) {


            if ($('#trigger').val() != true) {
                //sendForm();
            }
            if (isNaN(form.summa.value) || form.summa.value >= 0) {
                if (form.age.value >= 25 || form.age.value == "больше 70") {
                    if (form.obl.value != 'Донецкая область' && form.obl.value != 'Луганская область' && form.obl.value != 'Черновицкая область') {
                        if (form.tip.value == "Я работаю официально" && form.work_duration.value == "официально больше 3"
                            || form.tip.value == "Я получаю пенсию"
                            || form.tip.value == "Я частный предприниматель" && form.reg_duration.value == "больше 1 года") {
                            form.ok.value = 'order2';
                        }
                    }
                }
            }
        }


        if (d == 1) {
            $(form.form_button).attr('disabled', 'disabled');
            form.form_button.textContent = 'Отправка...';
            form.phone.value = phone_val;
            form.phone2.value = phone2_val;
            if ($('#trigger').val() != true) {
                //setTimeout(function() {form.submit()}, 1000);
                form.submit()
            } else {//setTimeout(function() {form.submit()}, 500);
                form.submit()
            }
            return false;
        }
        else {
            return false;
        }
        ;

    }
    ;

    function sendForm() {

        var orderdata = {};

        $('.for-ajax').each(function (index, value) {
            if ($(value)[0].tagName.toLowerCase() == 'input' && ($(value).attr('type') == 'radio' || $(value).attr('type') == 'checkbox') && $(value).is(':checked')) {
                orderdata[$(value).attr('name')] = $(value).attr('data-value');
            } else if ($(value).attr('type') != 'radio' && $(value).attr('type') != 'checkbox') {
                orderdata[$(value).attr('name')] = $(value).val();
            }
        });
        //console.log(orderdata);
        $.ajax({
            type: "POST",
            url: "_sending.php",
            dataType: "json",
            data: orderdata,
            beforeSend: function () {
                $('#trigger').val(true);
                //$('#thank').modal('show');
            },
            success: function (data) {


            }
        });
    }


    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    jQuery(function ($) {
        $('input[data-id="phone1"]').mask("999");
        $('input[data-id="phone1_2"]').mask("999");
        $('input[data-id="phone2"]').mask("999-99-99");
        $('input[data-id="phone2_2"]').mask("999-99-99");
        $("#code").mask("9999999999");
        if (isMobile.any()) {
            $("#device").val("Телефон");
        } else {
            $("#device").val("Компьютер");
        }
    });
</script>

