<div class="modal" tabindex="-1" role="dialog" id="documents">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Для ознакомления</h4>
            </div>
            <div class="modal-body">
                <div class="modal-desk">
                    Наши договора с банками партнерами и лицензии
                </div>
                <div class="bank_license">
                    - № ССС/14/09982 от 03.10.2014 г., лицензия НБУ № 61 от 05.10.2011г.<br/>
                    - № 2 от 15.06.2016 г. лицензия НБУ № 237 от 02.11.2011г.<br/>
                    - № 1 от 09.07.2015  г. лицензия НБУ № 96 от 04.11.2011г.
                </div>
                <button type="button" class="submit" data-dismiss="modal" aria-label="Close">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->