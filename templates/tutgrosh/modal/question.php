<div class="modal" tabindex="-1" role="dialog" id="question">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Задать вопрос</h4>
            </div>
            <div class="modal-body">
                <div class="modal-desk">
                    Заполните поля, ответим в самое<br/> ближайшее время!
                </div>
                <form class="callback_form quest">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="name" id="full_name" placeholder="Ваше Имя" required>
                        </div>
                        <div class="col-md-12">
                            <div class="phone_field">
                                <input name="phone1" type="text" id="phone1" placeholder="Номер телефона">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="email" name="mail" id="mail" placeholder="Электронная почта">
                        </div>
                        <div class="col-md-12">
                            <textarea name="message" id="message" placeholder="Напишите вопрос"></textarea>
                        </div>
                        <div class="col-md-12">
                            <input type="hidden" name="type" value="Вопрос">
                            <button type="submit" class="submit">Жду ответ</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->