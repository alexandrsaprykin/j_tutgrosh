<div class="modal" tabindex="-1" role="dialog" id="recall">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Перезвоните мне</h4>
            </div>
            <div class="modal-body">
                <div class="modal-desk">
                    Заполните поля, перезвоним в самое
                    ближайшее время!
                </div>
                <form class="second_form callme">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="name" id="full_name" placeholder="Ваше Имя" required>
                        </div>
                        <div class="col-md-12">
                            <div class="phone_field">
                                <input name="phone1" type="text" id="phone1" placeholder="Номер телефона" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="hidden" name="type" value="Перезвоните мне">
                            <button type="submit" class="submit">Жду звонка</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->