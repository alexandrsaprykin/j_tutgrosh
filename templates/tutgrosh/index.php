<?php

defined('_JEXEC') or die;

include_once "getreferer.php";

$app = JFactory::getApplication();
$menu = $app->getMenu();
$frontPage = $menu->getActive() == $menu->getDefault();
$page = $app->input->get('page');

$monthes = array(
	1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля',
	5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа',
	9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря'
);

$title = '';
if (!$page && $frontPage) {
	$title = 'Тутгрошi';
} elseif ($page == 'cart') {
	$title = 'Тутгрошi | Кредитная карта';
} elseif ($page == 'credit') {
	$title = 'Тутгрошi | Наличный кредит';
} elseif ($page == 'order') {
	$title = 'Тутгрошi | Спасибо за заявку';
} elseif ($page == 'confirm_page') {
	$title = 'Тутгрошi | SMS';
}

$ref = $_SERVER['REQUEST_URI'];
if (strpos($ref, 'utm_campaign')) {
	$istok = substr($ref, strpos($ref, 'utm_campaign') + 13);
	if (strpos($istok, '&')) {
		$istok = substr($istok, 0, strpos($istok, '&'));
		setcookie("istok", $istok);
	}
} else {
	$istok = 'Источник не определен';
	if (isset($_COOKIE['istok']) && $_COOKIE['istok']) {
		$istok = $_COOKIE['istok'];
	}
}
//var_dump($frontPage);
//$app = JFactory::getApplication('site');
//$template = $app->getTemplate('beez3');
//$param = $template->params->get('paramName', 'defaultValue');
?>
<!doctype html>
<!--[if lt IE 7]>
<html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]>
<html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]>
<html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->

<html lang="<?php echo $this->language; ?>">
<!--<![endif]-->
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113997124-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-113997124-1');
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Cache-Control" content="max-age=3600, must-revalidate">
    <title><?php echo $title ?></title>
    <link rel="icon" type="image/png"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/logo-credit.png"/>
    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/font-awesome-4.2.0/css/font-awesome.min.css"/>
    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/owl-carousel/owl.carousel.css">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/owl-carousel/owl.theme.css">
    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/slick/slick.css"/>
    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/fancybox/jquery.fancybox.css"/>
    <link rel="stylesheet"
          href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/jquery-ui-1.12.1.custom/jquery-ui.min.css"/>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/fonts.css"/>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/main.css"/>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/media.css"/>

    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/jquery/jquery-3.2.1.min.js"></script>
    <link rel="manifest" href="/manifest.json">
</head>

<body>


<?php if (!$page && $frontPage) {
	include 'main.php';
} elseif ($page == 'cart') {
	include 'cart.php';
} elseif ($page == 'credit') {
	include 'credit.php';
} elseif ($page == 'order') {
	include 'order.php';
} elseif ($page == 'confirm_page') {
	include 'confirmation_page.php';
} else { ?>
    <jdoc:include type="component"/>
<? } ?>




<?php include 'modal/feedback.php'; ?>
<?php include 'modal/recall.php'; ?>
<?php include 'modal/agree.php'; ?>
<?php include 'modal/documents.php'; ?>
<?php include 'modal/question.php'; ?>
<?php include 'modal/documents.php'; ?>

<!-- Оповещение ожидайте звонка -->
<div class="wait_phone">
    <h3>Ожидайте звонка!</h3>
</div>
<!-- Оповещение вопроса -->
<div class="wait_question">
    <h3>Спасибо за ваш вопрос!</h3>
</div>
<!-- Оповещение отзыва -->
<div class="wait_feed">
    <h3>Спасибо за ваш отзыв!</h3>
</div>


<!--[if lt IE 9]>

<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/html5shiv/es5-shim.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/html5shiv/html5shiv.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/html5shiv/html5shiv-printshiv.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/respond/respond.min.js"></script>
<![endif]-->


<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/inputmask/jquery.maskedinput.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/owl-carousel/owl.carousel.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/slick/slick.min.js"></script>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/libs/fancybox/jquery.fancybox.pack.js"></script>

<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/common.js"></script>

<script>!function(e,t,d,s,a,n,c){e[a]={},e[a].date=(new Date).getTime(),n=t.createElement(d),c=t.getElementsByTagName(d)[0],n.type="text/javascript",n.async=!0,n.src=s,c.parentNode.insertBefore(n,c)}(window,document,"script","https://tutgroshicomua.push.world/https.embed.js","pw"),pw.websiteId="f90db87b42352a25a2759e806232b1c7a44dccd87c951abd39e683b521ee61ff";</script>

</body>
</html>
