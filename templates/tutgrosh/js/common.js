$(document).ready(function () {

    //Modal
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    });

    //SLICk
    $('.slick').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    //Каруселька
    //Каруселька
    //Документация: http://owlgraphic.com/owlcarousel/
    var owl = $(".carousel");
    owl.owlCarousel({
        items: 6
    });

    $(".banks_right").click(function () {
        owl.trigger("owl.next");
    });
    $(".banks_left").click(function () {
        owl.trigger("owl.prev");
    });

    //  var owl = $(".feedback_slider");
    //  owl.owlCarousel({
    //  	items : 2
    //  });

    // $(".forward").click(function(){
    //  	owl.trigger("owl.next");
    //  });
    //  $(".prev").click(function(){
    //  	owl.trigger("owl.prev");
    // });

    //Range
    $(function () {
        $("#slider-range-min").slider({
            range: "min",
            value: 20000,
            min: 1000,
            step: 1000,
            max: 200000,
            slide: function (event, ui) {
                $("#amount").val(ui.value);
            },
            stop: function (event, ui) {
                calc2();
            }
        });
        $("#amount").val($("#slider-range-min").slider("value"));
    });
    $(function () {
        $("#slider-range-min2").slider({
            range: "min",
            value: 60,
            min: 1,
            step: 1,
            max: 60,
            slide: function (event, ui) {
                $("#amount2").val(ui.value);
            },
            stop: function (event, ui) {
                calc2();
            }
        });
        $("#amount2").val($("#slider-range-min2").slider("value"));
    });
// 	 <p>
//   <label for="amount">Maximum price:</label>
//   <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
// </p>

// <div id="slider-range-min"></div>
    function calc2() {

        var sk = $("#amount").val();
        var m = $("#amount2").val();
        $("#amount").attr('data-changed', 1);
        var ps = 8 / (100 * 12);
        var ps2 = 13 / (100 * 12);

//var k = Math.pow((i+1), n)*i/(Math.pow((i+1), n)-1);
//var a = Math.round(k*s);

        var ap = (sk * ps) / (1 - Math.pow((1 + ps), -m));
        var ap2 = (sk * ps2) / (1 - Math.pow((1 + ps2), -m));

        document.getElementById("calc_rez").innerHTML = Math.round(ap);
        document.getElementById("calc_rez2").innerHTML = Math.round(ap2);

    }

    $(document).on('click', '#calc_button', function () {
        if ($("#amount").attr('data-changed') == 1) {
            $('input#sum').val($('#amount').val());
        }
    });
    //Аякс отправка форм
    //Документация: http://api.jquery.com/jquery.ajax/

    $('#code,#sum,#phone1,#phone2').keypress(function (e) {
        if (e.keyCode < 48 || e.keyCode > 57) {
            console.log('NaN');
            return false;
        }
        ;
    });

    $(".callme").submit(function () {
        $.ajax({
            type: "POST",
            url: "/index.php?option=com_sender&view=form&task=form.mail",
            data: $(this).serialize()
        }).done(function () {
            $('.wait_phone').fadeIn();
            setTimeout(function () {
                $('.modal').fadeOut();
                $('.modal-backdrop.in').fadeOut();
                $('.wait_phone').fadeOut();
            }, 2000);
        });
        return false;
    });
    $(".feedapply").submit(function () {
        $.ajax({
            type: "POST",
            url: "/index.php?option=com_sender&view=form&task=form.mail",
            data: $(this).serialize()
        }).done(function () {
            $('.wait_feed').fadeIn();
            setTimeout(function () {
                $('.modal').fadeOut();
                $('.modal-backdrop.in').fadeOut();
                $('.wait_feed').fadeOut();
            }, 2000);
        });
        return false;
    });
    $(".quest").submit(function () {
        console.log($(this).serialize());
        $.ajax({
            type: "POST",
            url: "/index.php?option=com_sender&view=form&task=form.mail",
            data: $(this).serialize()
        }).done(function () {
            $('.wait_question').fadeIn();
            setTimeout(function () {
                $('.modal').fadeOut();
                $('.modal-backdrop.in').fadeOut();
                $('.wait_question').fadeOut();
            }, 2000);
        });
        return false;
    });

    $(".sms_form").submit(function () {
        $('.sms_form .submit').attr("disabled", 'disabled');
        $('.sms_form .submit').text('Проверка...');
        var orderdata = {};
        $('.for-ajax-sms').each(function (index, value) {
            if ($(value)[0].tagName.toLowerCase() == 'input' && ($(value).attr('type') == 'radio' || $(value).attr('type') == 'checkbox') && $(value).is(':checked')) {
                orderdata[$(value).attr('data-name')] = $(value).val();
            } else if ($(value).attr('type') != 'radio' && $(value).attr('type') != 'checkbox') {
                orderdata[$(value).attr('data-name')] = $(value).val();
            }
        });

        $.ajax({
            type: "POST",
            url: "/?option=com_sender&view=form&task=form.smsConfirm",
            dataType: "json",
            data: orderdata
        }).done(function (data) {
            if (data['success'] == true) {
                location = '/?page=order&ok=order2';
                setTimeout(function () {
                    $('#sms-confirm').css('display', 'none');
                    $('#empty_screen').removeClass('sms-confirm');
                }, 2000);
            } else {
                $('.sms_form #code_sms').val('');
                $('.sms_form .submit').text('Отправить');
                $('.sms_form .submit').removeAttr("disabled");
                $('.sms_form .error').css('display', 'block');
            }


        });
        return false;
    });
    $(document).on('change', '#tip', function () {
        if ($('#tip').val() == 'Я работаю официально') {
            $('#tip3').css('display', 'none').removeAttr('required');
            $('#tip2').css('display', 'block').attr('required', 'required');
        } else if ($('#tip').val() == 'Я частный предприниматель') {
            $('#tip2').css('display', 'none').removeAttr('required');
            $('#tip3').css('display', 'block').attr('required', 'required');
        } else {
            $('#tip3,#tip2').css('display', 'none').removeAttr('required');
        }

    });

});
